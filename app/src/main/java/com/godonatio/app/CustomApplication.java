package com.godonatio.app;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Christian Tamakloe on 04/04/2016.
 */
public class CustomApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // Init JodaTime
        JodaTimeAndroid.init(this);

        // Init custom fonts
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/source_sans_pro/SourceSansPro-Regular.ttf")
//                        .setDefaultFontPath("fonts/lato/Lato-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
