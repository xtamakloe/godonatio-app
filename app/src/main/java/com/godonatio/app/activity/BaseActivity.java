package com.godonatio.app.activity;


import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.godonatio.app.R;
import com.godonatio.app.interfaces.IUIEventListener;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.context.IconicsLayoutInflater;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Christian Tamakloe on 04/04/2016.
 */
public class BaseActivity
        extends AppCompatActivity
        implements IUIEventListener {

    MaterialDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Enable iconics font icons
        LayoutInflaterCompat.setFactory(getLayoutInflater(),
                new IconicsLayoutInflater(getDelegate()));
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        // Enable custom fonts
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onSnackBar(String message) {
        Snackbar snackbar;
        snackbar = Snackbar.make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        int bgColor = ContextCompat.getColor(getApplicationContext(), R.color.accent);
        snackBarView.setBackgroundColor(bgColor);

        int textColor = ContextCompat.getColor(getApplicationContext(), R.color.white);
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(textColor);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
        snackbar.show();
    }


    @Override
    public void onShowProgressDialog(int titleResId) {
        mProgressDialog = new MaterialDialog.Builder(this)
                .title(titleResId)
                .content(R.string.progress_dialog_content)
                .progress(true, 0)
                .backgroundColor(ContextCompat.getColor(this, R.color.primary))
                .titleColor(ContextCompat.getColor(this, R.color.white))
                .contentColor(ContextCompat.getColor(this, R.color.white))
                .widgetColor(ContextCompat.getColor(this, R.color.white))
                .build();
        mProgressDialog.show();
    }


    @Override
    public void onHideProgressDialog() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }


    @Override
    public void onShowInfoDialog(String title, String content) {
        new MaterialDialog.Builder(this)
                .title(title)
                .content(content)
                .icon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_info)
                        .color(ContextCompat.getColor(getApplicationContext(), R.color.primary))
                        .sizeDp(24)
                )
                .neutralText(getResources().getString(R.string.info_dialog_neutral_text))
                .show();
    }


    public void showAlert(int stringResourceId) {
        showAlert(stringResourceId);
    }


    public void showAlert(String message) {
        onSnackBar(message);
    }


    public void showInfoDialog(String title, String content) {
        onShowInfoDialog(title, content);
    }


    public void showInfoDialog(int title, int content) {
        onShowInfoDialog(getResources().getString(title), getResources().getString(content));
    }


    public void showProgressDialog(int title) {
        onShowProgressDialog(title);
    }


    public void hideProgressDialog() {
        onHideProgressDialog();
    }
}
