package com.godonatio.app.activity;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.godonatio.app.R;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.databinding.ActivityCreateContributionBinding;
import com.godonatio.app.event.RefreshProjectContributionsEvent;
import com.godonatio.app.event.RefreshProjectsListEvent;
import com.godonatio.app.fragment.ProjectFragment;
import com.godonatio.app.fragment.ProjectsListFragment;
import com.godonatio.app.model.db.ContributionDM;
import com.godonatio.app.model.view.ProjectVM;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.ImageUtils;
import com.godonatio.app.util.NetworkUtils;
import com.godonatio.app.util.SecureUtil;
import com.godonatio.app.util.UIUtils;
import com.google.gson.JsonObject;
import com.satsuware.usefulviews.LabelledSpinner;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateContributionActivity extends BaseActivity {

    static final int REQUEST_TAKE_PHOTO = 1;
    private static final String TAG = "DBG_CreateContributionActivity";
    private static final String PROJECT_LIST_SCREEN = "projectListScreen";
    private static final String PROJECT_SCREEN = "projectScreen";
    private static final int CONTRIBUTON_PIC_CAMERA_REQUEST = 12;
    private static final String KEY_CONTRIBUTION_TYPE = "contributionType";
    private static final String KEY_CONTRIBUTION_TYPE_POSITION = "contributionTypePosition";
    private static final String KEY_CONTRIBUTION_PIC_BITMAP = "contributionPicBitmap";
    private static final String KEY_CONTRIBUTION_PIC_FILE_PATH = "contributionFilePath";
    private static final String KEY_CONTRIBUTION_PIC_BYTE_ARRAY = "contributionPicByteArray";
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.et_amount)
    TextInputEditText mAmountField;
    @Bind(R.id.et_contributor_name)
    TextInputEditText mContributorNameField;
    @Bind(R.id.et_contributor_phone)
    TextInputEditText mContributorPhoneField;
    @Bind(R.id.et_message)
    TextInputEditText mMessageField;
    @Bind(R.id.ls_project)
    LabelledSpinner mProjectSpinner;
    @Bind(R.id.ls_contribution_type)
    LabelledSpinner mContributionTypeSpinner;
    @Bind(R.id.iv_contribution_pic)
    ImageView mPicImageView;
    @Bind(R.id.tv_contribution_pic_label)
    TextView mPicLabel;

    private String mContributionType;
    private ActivityCreateContributionBinding mBinding;
    private String[] mContributionTypes;
    private ProjectVM mProjectVM;
    private long mProjectId;
    private ArrayList<String> mProjectNames;
    private List<Long> mProjectIds;
    private String mContributorName;
    private String mMessage;
    private String mAmount;
    private String mContributorPhoneNo;
    private int mContributionTypePosition;
    /*private Bitmap mPicBitmap;*/
    private String mPicFilePath; // file path
    private byte[] mPicByteArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(
                this, R.layout.activity_create_contribution);
        ButterKnife.bind(this);

        mContributionTypes = getResources().getStringArray(R.array.contribution_types);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.new_contribution));
        UIUtils.setHomeAsUpIndicator(mToolbar, this);

        // Set up spinners
        setupProject();
        setupContributionType();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save contribution type spinner details
        outState.putString(KEY_CONTRIBUTION_TYPE, mContributionType);
        outState.putInt(KEY_CONTRIBUTION_TYPE_POSITION, mContributionTypePosition);

        // Save pic
        outState.putByteArray(KEY_CONTRIBUTION_PIC_BYTE_ARRAY, mPicByteArray);
        outState.putString(KEY_CONTRIBUTION_PIC_FILE_PATH, mPicFilePath);

        // TODO: Save project spinner details
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Restore contribution type spinner
        mContributionType = savedInstanceState.getString(KEY_CONTRIBUTION_TYPE);
        mContributionTypePosition = savedInstanceState.getInt(KEY_CONTRIBUTION_TYPE_POSITION);
        mContributionTypeSpinner.setSelection(mContributionTypePosition);

        // Restore picture if present
        mPicFilePath = savedInstanceState.getString(KEY_CONTRIBUTION_PIC_FILE_PATH);
        mPicByteArray = savedInstanceState.getByteArray(KEY_CONTRIBUTION_PIC_BYTE_ARRAY);
        showPicInImageView(mPicByteArray);

        // TODO: Restore project type spinner
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_contribution, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        UIUtils.hideSoftKeyboard(this); // hide keyboard
        switch (item.getItemId()) {
            case R.id.action_save_contribution:
                saveContribution();
                return true;

            /*case R.id.action_cancel:
                return true;*/

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setupContributionType() {
        mContributionTypePosition = 0;
        mContributionType = mContributionTypes[mContributionTypePosition].toLowerCase(); // Set first type as default

        LabelledSpinner contributionTypeSpinner =
                (LabelledSpinner) findViewById(R.id.ls_contribution_type);

        if (contributionTypeSpinner != null) {
            contributionTypeSpinner.setItemsArray(mContributionTypes);
            contributionTypeSpinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {

                @Override
                public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView,
                                         View itemView, int position, long id) {
                    mContributionTypePosition = position;
                    mContributionType = mContributionTypes[position].toLowerCase();

                    if (mContributionType.equalsIgnoreCase("gift")) {
                        mBinding.setShowAmount(false);
                        mAmountField.setText("");
                    } else
                        mBinding.setShowAmount(true);
                }


                @Override
                public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {
                    mContributionType = mContributionTypes[0].toLowerCase();
                }
            });
        }
    }


    private void setupProject() {
        // Check if project was specified
        mProjectVM = ProjectVM.from(getIntent().getStringExtra(ProjectFragment.KEY_PROJECT_JSON));
        if (mProjectVM != null) {

            mProjectId = mProjectVM.getId();

            mBinding.setShowProjects(false);

            return;
        }

        // If project wasn't specified, show all projects (if projects info is available)
        mProjectNames = getIntent().getStringArrayListExtra(
                ProjectsListFragment.KEY_PROJECT_NAMES);
        mProjectIds = (ArrayList<Long>) getIntent().getSerializableExtra(
                ProjectsListFragment.KEY_PROJECT_IDS);

        if (mProjectNames != null && mProjectIds != null
                && mProjectNames.size() > 0 && mProjectIds.size() > 0) {
            mBinding.setShowProjects(true);

            mProjectId = mProjectIds.get(0); // Set first project as default

            LabelledSpinner projectSpinner =
                    (LabelledSpinner) findViewById(R.id.ls_project);

            if (projectSpinner != null) {
                projectSpinner.setItemsArray(mProjectNames);
                projectSpinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
                    @Override
                    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView,
                                             View itemView, int position, long id) {
                        mProjectId = mProjectIds.get(position);
                    }


                    @Override
                    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {
                        mProjectId = mProjectIds.get(0);
                    }
                });
            }
        } else
            mBinding.setShowProjects(false);

        // TODO: check if default is set by onNothingChosen
    }


    public void onCaptureImageClick(View view) {
        dispatchTakePictureIntent();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONTRIBUTON_PIC_CAMERA_REQUEST && resultCode == RESULT_OK)
            processPic(mPicFilePath);
    }


    private void saveContribution() {
        // Get information from fields
        retrieveInputData();

        // Validate info
        if (!validateInputData())
            return;

        // Show dialog
        showConfirmationDialog();
    }


    private void retrieveInputData() {
        // NB: mContributionType and mProjectId already set
        mContributorName = mContributorNameField.getText().toString();
        mContributorPhoneNo = mContributorPhoneField.getText().toString();
        mAmount = mAmountField.getText().toString();
        mMessage = mMessageField.getText().toString();
    }


    public boolean validateInputData() {
        boolean valid = true;

        if (mProjectId == -1)
            showAlert("Project not set!");

        if (mContributionType == null)
            showAlert("Contribution type not set!");

        // Check name
        if (mContributorName.isEmpty()) {
            mContributorNameField.setError("Please enter a name");
            valid = false;
        } else
            mContributorNameField.setError(null);

        if (mContributorPhoneNo.length() < 9) {
            mContributorPhoneField.setError("You have not entered enough digits");
            valid = false;
        }

        // if selected contribution type is not gift, check for amount
        if (!mContributionType.equalsIgnoreCase("gift")) {
            if (mAmount.isEmpty()) {
                mAmountField.setError("Enter an amount");
                valid = false;
            }
        }

        return valid;
    }


    private void showConfirmationDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.create_contribution_title)
                .content(generateConfirmationMessage())
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // save to db
                        ContributionDM contribution = new ContributionDM();

                        /*contribution.setProject(project);
                        contribution.setAgent(agent);*/
                        contribution.setContributionType(mContributionType);
                        contribution.setContributorName(mContributorName);
                        contribution.setContributorPhoneNo(mContributorPhoneNo);
                        contribution.setRemarks(mMessage);
                        if (!mAmount.isEmpty())
                            contribution.setAmount(Float.parseFloat(mAmount));
                        contribution.setSignature(SecureUtil.generateUniqueString());
                        contribution.setCreatedAt(System.currentTimeMillis());
                        contribution.setCurrency("GHS"); //project.getDefaultCurrency()
                        contribution.setSynced(false);
                        // contribution.save();

                        // if (mSessionMgr.isOnlineMode())
                        saveOnline(mProjectId, contribution);
                        /*else
                            saveOffline(contribution);*/
                    }
                })
                .show();
    }


    private void saveOnline(long projectId, ContributionDM contribution) {
        // show progress dialog
        showProgressDialog(R.string.saving_contribution);

        makeAPICall(projectId, contribution);
    }


    private Call<APIResponse> getClient(long projectId, ContributionDM contribution) {
        APIService.ApiInterface client = APIService.getClient(this);

        if (mContributionType.equalsIgnoreCase("gift") && mPicFilePath != null) {
//            File resizedImage = resizeImage();

            RequestBody rqbFile = RequestBody.create(
                    MediaType.parse("image/jpg"), new File(mPicFilePath));
            /*RequestBody rqbFile = RequestBody.create(
                    MediaType.parse("image/jpg"), resizedImage);*/
            RequestBody rqbDesc = RequestBody.create(
                    MediaType.parse("multipart/form-data"), "Picture");

            return client.createContributionWithImage(
                    contribution.getContributorName(),
                    contribution.getContributorPhoneNo(),
                    projectId,
                    contribution.getContributionType(),
                    contribution.getSignature(),
                    contribution.getRemarks(),
                    contribution.getAmount(),
                    contribution.getCreatedAt(),
                    rqbFile,
                    rqbDesc
            );
        } else {
            JsonObject params = new JsonObject();
            params.addProperty("name", contribution.getContributorName());
            params.addProperty("msisdn", contribution.getContributorPhoneNo());
            params.addProperty("project_id", projectId);
            params.addProperty("type", contribution.getContributionType());
            params.addProperty("signature", contribution.getSignature());
            params.addProperty("remarks", contribution.getRemarks());
            params.addProperty("amount", contribution.getAmount());
            params.addProperty("received_at", contribution.getCreatedAt());

            return client.createContribution(params);
        }
    }


    private void makeAPICall(long projectId, ContributionDM contributionDM) {
        if (!NetworkUtils.isConnected(this))
            return;

        final Activity activity = this;

        getClient(projectId, contributionDM).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                hideProgressDialog(); // Dismiss progress dialog

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();
                    // if successful
                    if (apiResponse.isSuccess()) {
                        // nothing done locally
                        // just return to project screen and refresh it

                        int successAlertRes = R.string.saving_contribution_success;
                        // If project was available i.e. coming from project screen
                        // refresh project
                        if (mProjectVM != null)
                            EventBus.getDefault().post(
                                    new RefreshProjectContributionsEvent(
                                            mProjectId,
                                            successAlertRes));
                        else
                            EventBus.getDefault().post(
                                    new RefreshProjectsListEvent(
                                            getResources().getString(successAlertRes)
                                    ));
                        finish();

                    } else if (apiResponse.isError()) {
                        // TODO: showOperationFailedDialog(contribution,
                        // apiError.getMessage(), firstAttempt);
                        // for now, just show alert.
                        String errorMsg = getResources().getString(
                                R.string.saving_contribution_failed)
                                + apiResponse.getData().getError().getMessage();
                        showAlert(errorMsg);
                    } else
                        Log.d(TAG, "apiResponse else");

                } else {
                    APIUtils.handleRetrofitResponseBodyNull(activity, response);
                    // TODO: showOperationFailedDialog(contribution, errorMsg, firstAttempt);
                }
            }


            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                hideProgressDialog();
                // TODO: showOperationFailedDialog(contribution
                // ,"There is a problem with your Internet connection",firstAttempt);
                APIUtils.handleRetrofitFailure(activity, t);
            }
        });
    }


    private String generateConfirmationMessage() {

        String currency = (mProjectVM != null) ? mProjectVM.getDefaultCurrency() : "GHS";

        StringBuilder alertMsg = new StringBuilder();
        alertMsg.append("Accept ");

        if (!mContributionType.equalsIgnoreCase("gift"))
            alertMsg.append(String.format("%s %s ", currency, mAmount));

        alertMsg.append(String.format("%s from %s ", mContributionType, mContributorName));

        if (!mContributorPhoneNo.isEmpty())
            alertMsg.append(String.format("(%s)", mContributorPhoneNo));

        if (!mMessage.isEmpty())
            alertMsg.append(String.format(" with message \"%s\"?", mMessage));

        // Check for credits
        if (mProjectVM != null) {
            if (Float.parseFloat(mProjectVM.getAvailableCredits()) <= 0.0) {
                String noCreditMsg = String.format("\n\nNOTE: %s will not receive your receipt message "
                                + "because you have no credits. Please consider topping up."
                        , mContributorName);

                alertMsg.append(noCreditMsg);
            }
        }

        return alertMsg.toString();
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        mPicFilePath = "file:" + image.getAbsolutePath();
        mPicFilePath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, CONTRIBUTON_PIC_CAMERA_REQUEST);
            }
        }
    }


    void showPicInImageView(byte[] imageBytes) {
        if (imageBytes != null) {
            Bitmap bMap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            mPicImageView.setImageBitmap(bMap);
        }
    }


    private void processPic(final String filePath) {
        // Compress Image
        mPicByteArray = ImageUtils.compressImage(filePath);

        // Show pic in view
        showPicInImageView(mPicByteArray);

        // Write to file
        OutputStream fOut = null;
        try {
            fOut = new BufferedOutputStream(new FileOutputStream(filePath));
            fOut.write(mPicByteArray);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fOut != null)
                try {
                    fOut.flush();
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    /*void showFilePicInView(TextView labelView, ImageView view, String filePath) {
        labelView.setVisibility(View.GONE);
        view.setBackgroundResource(0);

        if (filePath != null) {
            File f = new File(filePath);
            view.setImageURI(Uri.fromFile(f));
        }
    }


    private File resizeImage() {
        Bitmap bMap = BitmapFactory.decodeFile(mPicFilePath);
        Bitmap out = Bitmap.createScaledBitmap(bMap, 300, 300, false);
        File resizedFile = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "resized.png");

        OutputStream fOut = null;
        try {
            fOut = new BufferedOutputStream(new FileOutputStream(resizedFile));
            out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            bMap.recycle();
            out.recycle();

            // Trying to be smart, not tested
            new File(mPicFilePath).delete(); // Delete old file
            mPicFilePath = resizedFile.getAbsolutePath(); // Point to new file to old variable

        } catch (Exception e) { // TODO
            e.printStackTrace();
        }
        return resizedFile;
    }


        // Save picture
        if (mPicBitmap != null) {
            ByteArrayOutputStream bStream = new ByteArrayOutputStream();
            *//*mPicBitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);*//*
            outState.putByteArray(KEY_CONTRIBUTION_PIC_BITMAP, bStream.toByteArray());
        }*/
}
