package com.godonatio.app.activity;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;

import com.godonatio.app.R;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.event.RefreshProjectEvent;
import com.godonatio.app.model.api.ProjectAM;
import com.godonatio.app.model.view.ProjectVM;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.NetworkUtils;
import com.godonatio.app.util.UIUtils;
import com.google.gson.Gson;
import com.satsuware.usefulviews.LabelledSpinner;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateUpdateProjectActivity extends BaseActivity {
    public static final String TAG = "DBG_CrUpProjActivity";
    private static final int EDIT_OPERATION = 2111;
    private static final int NEW_OPERATION = 2112;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.et_project_name)
    EditText mProjectName;
    @Bind(R.id.et_project_desc)
    EditText mProjectDescription;
    @Bind(R.id.et_project_receipt)
    EditText mProjectReceipt;
    /*@Bind(R.id.tv_group_pic_label)
    TextView mPicLabel;*/
    /*@Bind(R.id.iv_group_pic)
    ImageView mPicImageView;*/
    @Bind(R.id.sv_container_new_project)
    View mContainer;
    /*@Bind(R.id.pb_rotate)
    RotateLoading mProgressBar;*/
    private int mOperation;
    private ProjectVM mProjectVM;
    private String mProjectType;
    private String[] mProjectTypes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_update_project);

        ButterKnife.bind(this);

        mProjectTypes = getResources().getStringArray(R.array.project_types);

        String projectJSON = getIntent().getStringExtra(ProjectActivity.VM_JSON);
        mOperation = (projectJSON == null) ? NEW_OPERATION : EDIT_OPERATION;
        if (projectJSON != null) {
            mProjectVM = ProjectVM.from(projectJSON);
            mOperation = EDIT_OPERATION;
        } else
            mOperation = NEW_OPERATION;

        initView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_update_project, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_project_operation:
                saveProject();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    void initView() {
        setSupportActionBar(mToolbar);
        UIUtils.setHomeAsUpIndicator(mToolbar, this);

        // Set up spinner
        LabelledSpinner projectTypeSpinner = (LabelledSpinner) findViewById(R.id.ls_project_type);
        if (projectTypeSpinner != null) {
            projectTypeSpinner.setItemsArray(mProjectTypes);
            projectTypeSpinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
                @Override
                public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView,
                                         View itemView, int position, long id) {
                    mProjectType = mProjectTypes[position].toLowerCase();
                }


                @Override
                public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {
                    mProjectType = mProjectTypes[0].toLowerCase();
                }
            });
        }

        // Set up edit view
        if (isEditOperation()) {
            mProjectName.setText(mProjectVM.getName());
            mProjectDescription.setText(mProjectVM.getDescription());
        }

        // Set up new view
        if (isNewOperation()) {
            mProjectType = mProjectTypes[0];
        }
    }


    private void saveProject() {
        // Check form inputs
        if (!hasValidInputs())
            return;

        int title = 0;
        int message = 0;
        if (isNewOperation()) {
            title = R.string.add_project_dialog_title;
            message = R.string.add_project_dialog_message;
        }
        if (isEditOperation()) {
            title = R.string.edit_project_dialog_title;
            message = R.string.edit_project_dialog_message;
        }

        // Show confirmation alert TODO: convert to MaterialDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(title));
        builder.setMessage(getResources().getString(message));
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                makeAPICall();
            }
        });
        builder.setNegativeButton("NO", null);
        builder.show();
    }


    private void makeAPICall() {
        if (!NetworkUtils.isConnected(this))
            return;

        final String type = mProjectType;
        final String name = mProjectName.getText().toString();
        final String description = mProjectDescription.getText().toString();
        final String receipt = mProjectReceipt.getText().toString();

        /*String imagePath = (mPicPath != null) ? mPicPath : getDefaultImagePath(this);
        // "/data/data/com.artoconnect.whatgroups/files/placeholder_256.png"
        // Log.d("DBG_imagePath", imagePath);

        RequestBody rqbFile = RequestBody.create(
                MediaType.parse(getMimeType(imagePath)), new File(imagePath));*/

        if (!hasValidInputs())
            return;

        APIService.ApiInterface client = APIService.getClient(this);
        Call<APIResponse> call = null;

        int title;
        if (isEditOperation()) {
            call = client.updateProject(mProjectVM.getId(), name, type, description, receipt);
            title = R.string.update_project_dialog_title;
        } else if (isNewOperation()) {
            call = client.createProject(name, type, description, receipt);
            title = R.string.create_project_dialog_title;
        } else
            return;

        if (call == null)
            return;

        showProgressDialog(title);

        final Activity activity = this;

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                hideProgressDialog();

                if (!NetworkUtils.isAuthenticated(getApplicationContext(), response))
                    finish();

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();

                    if (apiResponse.isSuccess()) {

                        // Get project and convert to view model then to json for transport
                        ProjectAM projectAM = data.getProject();
                        String projectVMJSON = new Gson().toJson(ProjectVM.from(projectAM));

                        // After create operation, open new activity to show group
                        if (isNewOperation()) {
                            Intent intent = new Intent(getApplicationContext(), ProjectActivity.class);
                            // Convert project api model returned to view model and send
                            intent.putExtra(ProjectActivity.VM_JSON, projectVMJSON);
                            intent.putExtra(ProjectActivity.ALERT,
                                    projectAM.getName() + " created successfully");
                            startActivity(intent);

                        }
                        // After update operation, return to previous activity and refresh it
                        else if (isEditOperation()) {
                            // Refresh previous window => which is the group window
                            // should be a start activity for result
                            // so detach and reattach fragment to update it
//                                or i could just use eventbus
                            int alertResId = R.string.project_updated;

                            EventBus.getDefault().post(
                                    new RefreshProjectEvent(projectVMJSON, alertResId));
                        }
                        finish();

                        /* TODO:
                        // In both cases, refresh the home list of groups
                        EventBus.getDefault().post(new RefreshGroupListEvent());*/

                    } else if (apiResponse.isError())
                        showAlert(data.getError().getMessage());
                    else
                        Log.d(TAG, "Retrofit Else");

                } else  // response.body() == null
                    APIUtils.handleRetrofitResponseBodyNull(activity, response);

            }


            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                hideProgressDialog();

                APIUtils.handleRetrofitFailure(activity, t);
            }
        });


    }


    private boolean hasValidInputs() {
        boolean valid = true;
        // Group name is mandatory
        if (mProjectName.getText().toString().isEmpty()) {
            mProjectName.setError("Please enter a name for the group");
            valid = false;
        } else
            mProjectName.setError(null);
        return valid;
    }


    boolean isNewOperation() {
        return mOperation == NEW_OPERATION;
    }


    boolean isEditOperation() {
        return mOperation == EDIT_OPERATION;
    }

}
