package com.godonatio.app.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.godonatio.app.R;
import com.godonatio.app.adapter.MainActivityPagerAdapter;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.fragment.ProjectsListFragment;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.SessionManager;
import com.godonatio.app.util.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    private static final String TAG = "DBG_MainActivity";
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.vp_project_pages)
    ViewPager mViewPager;
    /*@Bind(R.id.tl_tab_titles)
    TabLayout mTabLayout;*/

    private SessionManager mSession;
    private MainActivityPagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        login();

        initView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // add new project
            case R.id.action_new_project:
                startActivity(new Intent(this, CreateUpdateProjectActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    void initView() {
        getSupportActionBar().setElevation(0);
        mViewPager.setOffscreenPageLimit(3);
        mAdapter = new MainActivityPagerAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(mAdapter);
        /*mTabLayout.setupWithViewPager(mViewPager);*/
    }


    void login() {
        // Setup session
        mSession = new SessionManager(this);
        if (!mSession.isValid())
            finish();
    }


    @OnClick(R.id.fab)
    void onFABClick() {
        // call a method in current fragment (ProjectListFragment)
        ((ProjectsListFragment) mAdapter.getCurrentFragment()).onFABClick();
    }
}
