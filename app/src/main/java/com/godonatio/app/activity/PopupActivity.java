package com.godonatio.app.activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import com.godonatio.app.R;
import com.squareup.picasso.Picasso;

public class PopupActivity extends AppCompatActivity {

    public static final String KEY_CONTRIBUTION_IMAGE_URL = "contributionImageUrl";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);

        // Set window dimensions
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = (int) (dm.widthPixels * 0.8);
        int height = (int) (dm.heightPixels * 0.6);

        getWindow().setLayout(width, height);

        // Load image
        String imageUrl = getIntent().getStringExtra(KEY_CONTRIBUTION_IMAGE_URL);
        if (imageUrl != null) {
            Picasso.with(this)
                    .load(imageUrl)
                    .resize(width, height)
                    .centerCrop()
                    .placeholder(R.drawable.gift_512)
                    .into((ImageView) findViewById(R.id.iv_popup_image));
        }
    }
}
