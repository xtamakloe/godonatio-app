package com.godonatio.app.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.godonatio.app.R;
import com.godonatio.app.fragment.ProjectFragment;

public class ProjectActivity extends BaseActivity {

    public static final String VM_JSON = "projectVMJSON";
    public static final String ALERT = "projectAlert";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);

        Fragment fragment = ProjectFragment.newInstance(
                getIntent().getStringExtra(VM_JSON), getIntent().getStringExtra(ALERT));
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment, ProjectFragment.TAG)
                .commit();
    }
}
