package com.godonatio.app.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.godonatio.app.R;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.databinding.ActivitySendMessageBinding;
import com.godonatio.app.event.RefreshProjectEvent;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.NetworkUtils;
import com.godonatio.app.util.UIUtils;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendMessageActivity extends BaseActivity {

    public static final String KEY_RECIPIENT_NAME = "recipientName";
    public static final String KEY_RECIPIENT_MSISDN = "recipientMsisdn";
    private static final String TAG = "DBG_SendMessageActivity";
    private static final String KEY_CONTRIBUTION_ID = "contributionId";
    private ActivitySendMessageBinding mBinding;
    private String mRecipientName;
    private String mRecipientMsisdn;
    private int mContributionId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_send_message);

        Toolbar toolbar = (Toolbar) mBinding.appbar.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.send_message_title);
        UIUtils.setHomeAsUpIndicator(toolbar, this);

        mRecipientName = getIntent().getStringExtra(KEY_RECIPIENT_NAME);
        mRecipientMsisdn = getIntent().getStringExtra(KEY_RECIPIENT_MSISDN);
        mContributionId = getIntent().getIntExtra(KEY_CONTRIBUTION_ID, -1);
        String instruction = "To: "
                + mRecipientName
                + " ("
                + mRecipientMsisdn
                + ")";
        mBinding.tvRecipient.setText(instruction);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_send_message, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send_message:
                showConfirmMessageDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void showConfirmMessageDialog() {
        // Check message
        if (!hasValidInputs())
            return;

        new MaterialDialog.Builder(this)
                .title(R.string.send_message_title)
                .content("Your message will be sent to " + mRecipientName)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        makeAPICall();
                    }
                })
                .show();
    }


    private void makeAPICall() {
        if (!NetworkUtils.isConnected(this))
            return;

        showProgressDialog(R.string.sending_message);

        String message = mBinding.etMessage.getText().toString();

        final Activity activity = this;

        APIService.ApiInterface client = APIService.getClient(this);
        client.sendMessage(
                mRecipientMsisdn,
                message,
                mContributionId
        ).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                hideProgressDialog();

                if (!NetworkUtils.isAuthenticated(getApplicationContext(), response))
                    finish();

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();

                    if (apiResponse.isSuccess()) {
                        // Refresh project to get updated contributions
                        EventBus.getDefault().post(
                                new RefreshProjectEvent(null, R.string.sending_message_success));

                        finish();

                    } else if (apiResponse.isError())
                        showAlert(apiResponse.getData().getError().getMessage());
                    else
                        Log.d(TAG, "not error not success");

                } else {
                    APIUtils.handleRetrofitResponseBodyNull(activity, response);
                }

            }


            @SuppressLint("LongLogTag")
            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                hideProgressDialog();

                Log.d(TAG, "Error:" + t.toString());

                showAlert("Something went wrong. Please try again in moment.");
            }
        });
    }


    private boolean hasValidInputs() {
        boolean valid = true;
        // Group name is mandatory
        EditText messageField = mBinding.etMessage;
        if (messageField.getText().toString().isEmpty()) {
            messageField.setError("Please enter a message");
            valid = false;
        } else
            messageField.setError(null);
        return valid;
    }
}
