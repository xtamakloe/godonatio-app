package com.godonatio.app.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.godonatio.app.R;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.fragment.LoginFragment;
import com.godonatio.app.interfaces.ISessionEventListener;
import com.godonatio.app.model.api.SessionAM;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.NetworkUtils;
import com.godonatio.app.util.SessionManager;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SessionActivity
        extends BaseActivity
        implements ISessionEventListener {

    private static final String TAG = "DBG_SessionActivity";
    String mFirstName;
    String mLastName;
    String mEmail;
    String mPassword;
    int mSessionMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_session);

        Fragment fragment = LoginFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.session_fragment_container, fragment, TAG)
                .commit();
    }


    @Override
    public void onProcessSessionRequest(int sessionMode) {
        mSessionMode = sessionMode;

        if (validateInputs())
            sendSessionRequest();
    }


    private boolean validateInputs() {
        boolean valid = true;

        EditText firstNameField = (EditText) findViewById(R.id.et_firstname);
        EditText lastNameField = (EditText) findViewById(R.id.et_lastname);
        EditText emailField = (EditText) findViewById(R.id.et_email);
        EditText passwordField = (EditText) findViewById(R.id.et_password);

        mEmail = emailField.getText().toString().trim();
        mPassword = passwordField.getText().toString().trim();

        if (mEmail.isEmpty()) {
            emailField.setError("Please enter a valid email");
            valid = false;
        } else
            emailField.setError(null);

        if (mPassword.isEmpty()) {
            passwordField.setError("Please enter a valid password");
            valid = false;
        } else
            passwordField.setError(null);

        if (isRegisterMode()) {
            // Check first name on register
            mFirstName = firstNameField.getText().toString().trim();
            mLastName = lastNameField.getText().toString().trim();

            if (mFirstName.isEmpty()) {
                firstNameField.setError("Please enter a valid name");
                valid = false;
            } else
                firstNameField.setError(null);
        }
        return valid;
    }


    void sendSessionRequest() {
        if (!NetworkUtils.isConnected(this))
            return;

        // on both login and register
        // call to create user, use that info to log in
        // registering...., logging in.... dialogs
        // log user in,
        // refresh db: create or update projects
        // list their current projects (pull from db or json response?)
        //      => some projects might be unsynced i.e. when created with no internet,
        //      so provide button to sync them later. so always list from db.

        int title = -1;
        APIService.ApiInterface client = APIService.getClient(this);
        Call<APIResponse> call = null;

        if (isRegisterMode()) {
            call = client.register(mFirstName, mLastName, mEmail, mPassword);
            title = R.string.sign_up_dialog_title;
        } else if (isLoginMode()) {
            call = client.login(mEmail, mPassword);
            title = R.string.log_in_dialog_title;
        } else
            return;

        // on response, if isRegisterMode() => call xxx this time with LOGIN
        showProgressDialog(title);

        final Activity activity = this;

        call.enqueue(new Callback<APIResponse>() {

            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                hideProgressDialog();

                if (!NetworkUtils.isAuthenticated(getApplicationContext(), response))
                    finish();

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();

                    if (apiResponse.isSuccess()) {

                        if (isRegisterMode()) {
                            // After registering user, log them in
                            mSessionMode = LOGIN;
                            sendSessionRequest();
                        } else {
                            Headers headers = response.headers();
                            String uid = headers.get(APIService.HEADER_UID);
                            String accessToken = headers.get(APIService.HEADER_ACCESS_TOKEN);
                            String clientId = headers.get(APIService.HEADER_CLIENT_ID);
                            SessionAM session = data.getSession();

                            // Start application
                            startApp(uid, accessToken, clientId, session);
                        }
                    } else if (apiResponse.isError()) {

                        Log.d(TAG, "Some errors here");

                        showAlert(data.getError().getMessage());
                    } else
                        Log.d(TAG, "apiResponse else");

                } else {
                    APIUtils.handleRetrofitResponseBodyNull(activity, response);
                }
            }


            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                hideProgressDialog();

                APIUtils.handleRetrofitFailure(activity, t);
            }
        });

    }


    void startApp(String uid, String accessToken, String clientId, SessionAM session) {
        new SessionManager(this).login(uid, accessToken, clientId, session);
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }


    boolean isRegisterMode() {
        return mSessionMode == REGISTER;
    }


    boolean isLoginMode() {
        return mSessionMode == LOGIN;
    }

}
