package com.godonatio.app.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.godonatio.app.BR;
import com.godonatio.app.R;
import com.godonatio.app.binding.ContributionsListItemBindingHandlers;
import com.godonatio.app.model.view.ContributionVM;

import java.util.List;

/**
 * Created by Christian Tamakloe on 16/04/2016.
 */
public class ContributionsListAdapter extends RecyclerView.Adapter<ContributionsListAdapter.ContributionItemBindingHolder> {
    private List<ContributionVM> mContributionVMs;

    public ContributionsListAdapter(List<ContributionVM> contributionVMs) {
        this.mContributionVMs = contributionVMs; // NB: Uses view models
    }


    @Override
    public ContributionItemBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_contribution, parent, false);
        return new ContributionItemBindingHolder(view);
    }


    @Override
    public void onBindViewHolder(ContributionItemBindingHolder holder, int position) {
        final ContributionVM contributionVM = mContributionVMs.get(position);

        holder.getBinding().setVariable(BR.contribution, contributionVM);
        holder.getBinding().setVariable(BR.handlers,
                new ContributionsListItemBindingHandlers(contributionVM));
        holder.getBinding().executePendingBindings();
    }


    @Override
    public int getItemCount() {
        return mContributionVMs.size();
    }


    public class ContributionItemBindingHolder extends RecyclerView.ViewHolder {

        private ViewDataBinding mBinding;

        public ContributionItemBindingHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }

        public ViewDataBinding getBinding() {
            return mBinding;
        }
    }
}
