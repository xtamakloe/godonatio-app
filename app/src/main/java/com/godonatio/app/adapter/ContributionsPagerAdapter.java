package com.godonatio.app.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.godonatio.app.R;
import com.godonatio.app.fragment.ContributionsListFragment;
import com.godonatio.app.fragment.ProjectsListFragment;
import com.godonatio.app.model.view.ProjectVM;

/**
 * Created by Christian Tamakloe on 15/04/2016.
 */
public class ContributionsPagerAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 1;
    private final ProjectVM mProjectVM;
    private final String mProjectJSON;


    public ContributionsPagerAdapter(FragmentManager supportFragmentManager, String projectJSON) {
        super(supportFragmentManager);
        mProjectVM = ProjectVM.from(projectJSON);
        mProjectJSON = projectJSON;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ContributionsListFragment.newInstance(mProjectJSON,
                        ContributionsListFragment.SAVED);
            /*case 0:
                return ContributionsListFragment.newInstance(mProjectVM.getId(),
                        ContributionsListFragment.SAVED);*/
            /*case 1:
                return ContributionsListFragment.newInstance(ContributionsListFragment.UNSAVED);*/
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
