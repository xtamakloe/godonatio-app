package com.godonatio.app.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.godonatio.app.R;
import com.godonatio.app.fragment.ProjectsListFragment;

/**
 * Created by Christian Tamakloe on 14/04/2016.
 */
public class MainActivityPagerAdapter extends FragmentStatePagerAdapter {


    final int PAGE_COUNT = 4;
    private Context mContext;
    private Integer[] mTabTitles = new Integer[]{
            R.string.tab_title_events,
            R.string.tab_title_groups,
            R.string.tab_title_causes,
            R.string.tab_title_payments
    };
    private Fragment mCurrentFragment;

    public MainActivityPagerAdapter(FragmentManager supportFragmentManager,
                                    Context context) {
        super(supportFragmentManager);
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ProjectsListFragment.newInstance(ProjectsListFragment.EVENTS);
            case 1:
                return ProjectsListFragment.newInstance(ProjectsListFragment.GROUPS);
            case 2:
                return ProjectsListFragment.newInstance(ProjectsListFragment.CAUSES);
            case 3:
                return ProjectsListFragment.newInstance(ProjectsListFragment.PAYMENTS);
            default:
                return ProjectsListFragment.newInstance(ProjectsListFragment.EVENTS);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(mTabTitles[position]);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (mCurrentFragment != object)
            mCurrentFragment = (Fragment) object;
        super.setPrimaryItem(container, position, object);
    }


    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }
}
