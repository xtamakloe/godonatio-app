package com.godonatio.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.godonatio.app.fragment.ProjectInfoFragment;

/**
 * Created by Christian Tamakloe on 14/04/2016.
 */
public class ProjectInfoPagerAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 2;
    private final String mProjectJSON;

    public ProjectInfoPagerAdapter(FragmentManager supportFragmentManager, String projectJSON) {
        super(supportFragmentManager);
        this.mProjectJSON = projectJSON;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ProjectInfoFragment.newInstance(
                        ProjectInfoFragment.PROJECT_SUMMARY, mProjectJSON);
            case 1:
                return ProjectInfoFragment.newInstance(
                        ProjectInfoFragment.PROJECT_DETAILS, mProjectJSON);
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
