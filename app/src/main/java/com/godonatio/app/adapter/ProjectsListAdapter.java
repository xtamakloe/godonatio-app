package com.godonatio.app.adapter;

import android.databinding.DataBindingUtil;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.godonatio.app.BR;
import com.godonatio.app.R;
import com.godonatio.app.binding.ProjectsListItemBindingHandlers;
import com.godonatio.app.model.view.ProjectVM;

import java.util.List;

/**
 * Created by Christian Tamakloe on 13/04/2016.
 */
public class ProjectsListAdapter extends RecyclerView.Adapter<ProjectsListAdapter.ProjectItemBindingHolder> {

    private List<ProjectVM> mProjectVMs;


    public ProjectsListAdapter(List<ProjectVM> projectVMs) {
        this.mProjectVMs = projectVMs; // NB: Uses view models
    }


    @Override
    public ProjectItemBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_project, parent, false);
        return new ProjectItemBindingHolder(view);
    }


    @Override
    public void onBindViewHolder(ProjectItemBindingHolder holder, int position) {
        final ProjectVM projectVM = mProjectVMs.get(position);

        holder.getBinding().setVariable(BR.project, projectVM);
        holder.getBinding().setVariable(BR.handlers, new ProjectsListItemBindingHandlers(projectVM));
        holder.getBinding().executePendingBindings();
    }


    @Override
    public int getItemCount() {
        return mProjectVMs.size();
    }


    public class ProjectItemBindingHolder extends RecyclerView.ViewHolder {

        private ViewDataBinding mBinding;

        public ProjectItemBindingHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }

        public ViewDataBinding getBinding() {
            return mBinding;
        }
    }
}
