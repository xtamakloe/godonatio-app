package com.godonatio.app.api;

/**
 * Created by Christian Tamakloe on 10/04/2016.
 */
public class APIError {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
