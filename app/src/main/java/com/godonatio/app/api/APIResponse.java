package com.godonatio.app.api;


import com.godonatio.app.model.api.ContributionAM;
import com.godonatio.app.model.api.ProjectAM;
import com.godonatio.app.model.api.SessionAM;

import java.util.List;

/**
 * Created by Christian Tamakloe on 10/04/2016.
 */
public class APIResponse {
    private String status;
    private Data data;


    public boolean isSuccess() {
        return status != null && status.equals("success");
    }


    public boolean isFail() {
        return status != null && status.equals("fail");
    }


    public boolean isError() {
        return status != null && status.equals("error");
    }


    public Data getData() {
        return data;
    }


    public class Data {
        APIError error;
        SessionAM session;
        ProjectAM project;
        private List<ProjectAM> projects;
        private List<ContributionAM> contributions;


        public APIError getError() {
            return error;
        }


        public SessionAM getSession() {
            return session;
        }


        public ProjectAM getProject() {
            return project;
        }


        public List<ProjectAM> getProjects() {
            return projects;
        }


        public List<ContributionAM> getContributions() {
            return contributions;
        }
    }
}
