package com.godonatio.app.api;


import android.content.Context;
import android.util.Log;

import com.godonatio.app.util.SessionManager;
import com.google.gson.JsonObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by Christian Tamakloe on 10/04/2016.
 */
public class APIService {
//        public static final String API_BASE_URL = "http://10.0.3.2:3000/api/v1/";
    public static final String API_BASE_URL = "http://godonatio.herokuapp.com/api/v1/";
    public static final String HEADER_ACCESS_TOKEN = "X-Access-Token";
    public static final String HEADER_UID = "X-Uid";
    public static final String HEADER_CLIENT_ID = "X-Client";
    private static final String TAG = "DBG_APIService";
    private static ApiInterface sApiInterface;
    private static Context sContext;


    public static ApiInterface getClient(Context context) {
        sContext = context;
        if (sApiInterface == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .client(getOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            sApiInterface = retrofit.create(ApiInterface.class);
        }
        return sApiInterface;
    }


    public static OkHttpClient getOkHttpClient() {
        // http://inthecheesefactory.com/blog/retrofit-2.0/en
        OkHttpClient.Builder client = new OkHttpClient.Builder();

        // Authenticate request
        addAuthHeadersInterceptor(client);

        // Log response
        addLoggingInterceptor(client);

        return client.build();
    }


    /* Interceptor for adding auth headers */
    static OkHttpClient.Builder addAuthHeadersInterceptor(final OkHttpClient.Builder client) {
        client.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                SessionManager sessionMgr = new SessionManager(sContext);

                String headerUid = sessionMgr.getUid();
                String clientId = sessionMgr.getClientId();
                String accessToken = sessionMgr.getToken();

                Request request = original.newBuilder()
                        .header(HEADER_UID, headerUid == null ? "" : headerUid)
                        .header(HEADER_CLIENT_ID, clientId == null ? "" : clientId)
                        .header(HEADER_ACCESS_TOKEN, accessToken == null ? "" : accessToken)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });
        return client;
    }


    /* Interceptor for logging results*/
    static OkHttpClient.Builder addLoggingInterceptor(OkHttpClient.Builder client) {
        client.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());

                String content = response.body().string();

                /*Log.d(TAG, "headers: " + response.headers().toString());
                Log.d(TAG, "message: " + response.message());
                Log.d(TAG, "code: " + Integer.toString(response.code()));*/
                Log.d(TAG, "content: " + content);

                // {"status"=>"error", "data"=>{"error"=>{"message"=>"Internal server error"}}}
                // {"status"=>"error", "data"=>{"error"=>{"message"=>"Member not found"}}}

                // Gson gson = new Gson();
                // APIResponse apiResponse = gson.fromJson(content, APIResponse.class);

                // Log.d("DBG", apiResponse.getData().getError().toString());
                // Log.d("DBG", Integer.toString(apiResponse.getData().getError().getCode()));

                // Return original
                return response.newBuilder()
                        .body(ResponseBody.create(response.body().contentType(), content))
                        .build();
            }
        }).build();

        return client;
    }


    /**
     * API Interface
     */
    public interface ApiInterface {

        /* Sessions */
        @POST("sessions")
        Call<APIResponse> login(
                @Query("email") String email,
                @Query("password") String password
        );

        @POST("sessions/register")
        Call<APIResponse> register(
                @Query("first_name") String lastName,
                @Query("last_name") String firstName,
                @Query("email") String email,
                @Query("password") String password
        );


        /* Projects */
        @GET("projects")
        Call<APIResponse> getUserProjects();

        @GET("projects?type=event")
        Call<APIResponse> getUserEventProjects();

        @GET("projects?type=cause")
        Call<APIResponse> getUserCauseProjects();

        @GET("projects?type=group")
        Call<APIResponse> getUserGroupProjects();

        @GET("projects?type=payment")
        Call<APIResponse> getUserPaymentProjects();

        @POST("projects")
        Call<APIResponse> createProject(
                @Query("name") String name,
                @Query("type") String type,
                @Query("description") String description,
                @Query("receipt_template") String template
        );

        @PUT("projects/{id}")
        Call<APIResponse> updateProject(
                @Path("id") long projectId,
                @Query("name") String name,
                @Query("type") String type,
                @Query("description") String description,
                @Query("receipt_template") String template
        );

        @PUT("projects/{id}")
        Call<APIResponse> toggleProjectStatus(
                @Path("id") long projectId,
                @Query("active") String status
        );

        @GET("projects/{id}")
        Call<APIResponse> getProject(@Path("id") long projectId);

        @DELETE("projects/{id}")
        Call<APIResponse> deleteProject(@Path("id") long projectId);


        /* Contributions */
        @GET("contributions")
        Call<APIResponse> getProjectContributions(@Query("project_id") long projectId);

        @POST("contributions")
        Call<APIResponse> createContribution(@Body JsonObject body);

        @Multipart
        @POST("contributions")
        Call<APIResponse> createContributionWithImage(
                @Query("name") String name,
                @Query("msisdn") String msisdn,
                @Query("project_id") long projectId,
                @Query("type") String type,
                @Query("signature") String signature,
                @Query("remarks") String remarks,
                @Query("amount") Float amount,
                @Query("received_at") long receivedAt,
                @Part("pic\"; filename=\"image.jpg\" ") RequestBody file,
                @Part("description") RequestBody imageDescription
        );

        @DELETE("contributions/{id}")
        Call<APIResponse> deleteContribution(@Path("id") long contributionId);


        /* Top-ups */
        @POST("top_ups")
        Call<APIResponse> createTopUp(
                @Query("project_id") long projectId,
                @Query("voucher_number") String voucherNumber
        );


        /* Messages */
        @POST("messages")
        Call<APIResponse> sendMessage(
                @Query("msisdn") String recipientMsisdn,
                @Query("message") String message,
                @Query("contribution_id") int contributionId
        );


        /* Others */

        /*
        // All groups
        @GET("groups")
        Call<APIResponse> getGroups();

        // Search groups
        @GET("groups")
        Call<APIResponse> getGroupsByUidOrName(
                @Query("query") String query
        );

        // Signups
        @GET("groups?admin=0")
        Call<APIResponse> getGroupsByMemberRequest(
                @Query("member_id") int memberId
        );

        // Added groups
        @GET("groups?admin=1")
        Call<APIResponse> getGroupsByMemberAdmin(
                @Query("member_id") int memberId
        );

        @Multipart
        @POST("groups")
        Call<APIResponse> createGroup(
                @Query("member_id") int memberId,
                @Query("group_name") String name,
                @Query("group_description") String groupDescription,
                @Part("pic\"; filename=\"image.png\" ") RequestBody file,
                @Part("description") RequestBody imageDescription
        );

        @Multipart
        @PUT("groups/{id}")
        Call<APIResponse> updateGroup(
                @Path("id") int groupId,
                @Query("member_id") int memberId,
                @Query("group_name") String name,
                @Query("group_description") String groupDescription,
                @Part("pic\"; filename=\"image.png\" ") RequestBody file,
                @Part("description") RequestBody imageDescription
        );

        @GET("groups/{id}")
        Call<APIResponse> getGroupInfo(
                @Path("id") int groupId
        );

        @DELETE("groups/{id}")
        Call<APIResponse> deleteGroup(
                @Path("id") int groupId,
                @Query("member_id") int memberId
        );

        @POST("memberships")
        Call<APIResponse> requestMembership(
                @Query("group_id") int groupId,
                @Query("member_id") int memberId
        );

        @DELETE("memberships")
        Call<APIResponse> cancelMembership(
                @Query("group_id") int groupId,
                @Query("member_id") int memberId
        );

        // should return memberId
        @POST("sessions")
        Call<APIResponse> createSession(
                @Query("phone_no") String phoneNo,
                @Query("password") String password
        );

        // should return memberId
        @POST("members")
        Call<APIResponse> createMember(
                @Query("username") String username,
                @Query("phone_no") String phoneNo,
                @Query("password") String password
        );

        */


    }
}

