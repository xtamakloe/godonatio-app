package com.godonatio.app.binding;


import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.widget.ImageView;

import com.godonatio.app.R;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.squareup.picasso.Picasso;

/**
 * Created by Christian Tamakloe on 28/04/2016.
 */
public class ContributionListBindingAdapter {
    private static final String TAG = "DBG_ContributionListBindingAdapter";

    /*@BindingAdapter("bind:icon")
    public static void loadImage(ImageView imageView, String url) {
        Picasso.with(imageView.getContext()).load(url).into(imageView);
    }*/


    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView imageView, String url) {
        Picasso.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.gift_512)
                .into(imageView);
    }

}
