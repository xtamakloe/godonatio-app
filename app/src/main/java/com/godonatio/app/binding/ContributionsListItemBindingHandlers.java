package com.godonatio.app.binding;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.godonatio.app.R;
import com.godonatio.app.activity.BaseActivity;
import com.godonatio.app.activity.PopupActivity;
import com.godonatio.app.activity.SendMessageActivity;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.event.RefreshProjectContributionsEvent;
import com.godonatio.app.model.view.ContributionVM;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.NetworkUtils;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Christian Tamakloe on 16/04/2016.
 */
public class ContributionsListItemBindingHandlers {

    private final ContributionVM mContributionVM;
    private Context mContext;


    public ContributionsListItemBindingHandlers(ContributionVM contributionVM) {
        mContributionVM = contributionVM;
    }


    public void onImageClick(View view) {
        mContext = view.getContext();
        Intent popupIntent = new Intent(mContext, PopupActivity.class);
        popupIntent.putExtra(PopupActivity.KEY_CONTRIBUTION_IMAGE_URL,
                mContributionVM.getPicUrlOriginal());
        mContext.startActivity(popupIntent);
    }


    public void onClick(View view) {
        mContext = view.getContext();
        showContributionDetailsDialog();
    }


    private void showAlert(String s) {
        ((BaseActivity) mContext).showAlert(s);
    }


    private void showContributionDetailsDialog() {
        new MaterialDialog.Builder(mContext)
                .title(mContributionVM.getContributorName().trim() + " gave")
                .content(generateContributionDetails())
                .negativeColorRes(R.color.red_500)
//                .positiveText(R.string.print_contribution)
                .negativeText(R.string.delete_contribution)
                .neutralText(R.string.contact_contributor)
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        showContactContributorOptionsDialog();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        showConfirmDeletionDialog();
                    }
                })
                .show();
    }


    private void showContactContributorOptionsDialog() {
        new MaterialDialog.Builder(mContext)
                .title(R.string.contact_contributor_dialog_title)
                .items(R.array.contact_types)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            // Make Phone Call
                            case 0:
                                callContributor();
                                break;
                            // Send message
                            case 1:
                                sendMessage();
                                break;
                            // Share gift news
                            case 2:
                                showAlert("Coming Soon...");
                                break;

                        }

                    }
                })
                .negativeText(R.string.contact_contributor_dialog_negative)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        showContributionDetailsDialog();
                    }
                })
                .show();
    }


    private void showConfirmDeletionDialog() {
        new MaterialDialog.Builder(mContext)
                .title(R.string.confirm_delete_contribution_dialog_title)
                .content(R.string.confirm_delete_contribution_dialog_content)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        deleteContribution();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        showContributionDetailsDialog();
                    }
                })
                .show();
    }


    private String generateContributionDetails() {
        StringBuilder content = new StringBuilder();

        if (mContributionVM.isCash())
            content.append("A cash amount of " + mContributionVM.getAmount());
        else if (mContributionVM.isCheque())
            content.append("A cheque for " + mContributionVM.getAmount());
        else
            content.append("A beautiful gift");

        if (mContributionVM.getRemarks().trim().length() > 0)
            content.append(" with the following message: \n"
                    + "\"" + mContributionVM.getRemarks()
                    + "\"");

        content.append("\n\nTime: " + mContributionVM.getReceivedAt());
        content.append("\nPhone: " + mContributionVM.getContributorMsisdn());

        return content.toString();
    }


    private void callContributor() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri number = Uri.parse("tel:" + mContributionVM.getContributorMsisdn());
        intent.setData(number);

        if (ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
            mContext.startActivity(intent);
    }


    private void sendMessage() {
        Intent intent = new Intent(mContext, SendMessageActivity.class);
        intent.putExtra(SendMessageActivity.KEY_RECIPIENT_NAME,
                mContributionVM.getContributorName());
        intent.putExtra(SendMessageActivity.KEY_RECIPIENT_MSISDN,
                mContributionVM.getContributorMsisdn());
        mContext.startActivity(intent);
    }


    private void deleteContribution() {
        final BaseActivity activity = (BaseActivity) mContext;

        activity.showProgressDialog(R.string.deleting_contribution);

        if (!NetworkUtils.isConnected(activity))
            return;

        APIService.ApiInterface client = APIService.getClient(activity);

        client.deleteContribution(mContributionVM.getId()).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                activity.hideProgressDialog(); // Dismiss progress dialog

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();
                    // if successful
                    if (apiResponse.isSuccess()) {
                        // nothing done locally
                        // just return to project screen and refresh it

                        // If project was available i.e. coming from project screen
                        // refresh project
                        EventBus.getDefault().post(
                                new RefreshProjectContributionsEvent(
                                        mContributionVM.getProjectId(),
                                        R.string.deleting_contribution_success));

                    } else if (apiResponse.isError()) {
                        // TODO: showOperationFailedDialog(contribution,
                        // apiError.getMessage(), firstAttempt);
                        // for now, just show alert.
                        String errorMsg = activity.getResources().getString(
                                R.string.deleting_contribution_failed);
                        activity.showAlert(errorMsg);
                    }
                } else
                    APIUtils.handleRetrofitResponseBodyNull(activity, response);
            }


            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                activity.hideProgressDialog();

                APIUtils.handleRetrofitFailure(activity, t);
            }
        });
    }


}
