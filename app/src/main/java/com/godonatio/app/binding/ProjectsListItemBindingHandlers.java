package com.godonatio.app.binding;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.godonatio.app.activity.ProjectActivity;
import com.godonatio.app.model.view.ProjectVM;
import com.google.gson.Gson;

/**
 * Created by Christian Tamakloe on 13/04/2016.
 */
public class ProjectsListItemBindingHandlers {
    private final ProjectVM mProjectVM;

    public ProjectsListItemBindingHandlers(ProjectVM projectVM) {
        this.mProjectVM = projectVM;
    }

    public void onClickProject(View view) {
        Context context = view.getContext();
        Intent intent = new Intent(context, ProjectActivity.class);
        // serialize project object to json and put into intent
        intent.putExtra(ProjectActivity.VM_JSON, new Gson().toJson(mProjectVM));
        context.startActivity(intent);
    }
}

