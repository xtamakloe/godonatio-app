package com.godonatio.app.event;


/**
 * Created by Christian Tamakloe on 26/04/2016.
 */
public class RefreshProjectContributionsEvent {
    public final Long mProjectId;
    public int mAlertResId = -1;


    public RefreshProjectContributionsEvent(Long projectId, int alertResId) {
        this.mProjectId = projectId;
        this.mAlertResId = alertResId;
    }


    public Long getProjectId() {
        return mProjectId;
    }


    public int getAlertResId() {
        return mAlertResId;
    }
}
