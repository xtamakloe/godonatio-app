package com.godonatio.app.event;


/**
 * Created by Christian Tamakloe on 26/04/2016.
 */
public class RefreshProjectEvent {
    private final String mProjectJSON;
    private final int mAlertResId;


    public RefreshProjectEvent(String projectJSON, int alertResId) {
        this.mProjectJSON = projectJSON;
        this.mAlertResId = alertResId;
    }


    public int getAlertResId() {
        return mAlertResId;
    }


    public String getProjectJSON() {
        return mProjectJSON;
    }
}
