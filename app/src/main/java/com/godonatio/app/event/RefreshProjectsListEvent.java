package com.godonatio.app.event;


import android.util.Log;

/**
 * Created by Christian Tamakloe on 26/04/2016.
 */
public class RefreshProjectsListEvent {

    private final String mAlertMsg;


    public RefreshProjectsListEvent(String alertMessage) {
        this.mAlertMsg = alertMessage;
    }


    public String getAlertMsg() {
        return mAlertMsg;
    }
}
