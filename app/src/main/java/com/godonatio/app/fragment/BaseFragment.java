package com.godonatio.app.fragment;


import android.support.v4.app.Fragment;

import com.godonatio.app.activity.BaseActivity;

/**
 * Created by Christian Tamakloe on 30/04/2016.
 */
public class BaseFragment extends Fragment {

    public void showAlert(int stringResourceId) {
        ((BaseActivity) getActivity()).onSnackBar(getResources().getString(stringResourceId));
    }


    public void showAlert(String message) {
        ((BaseActivity) getActivity()).onSnackBar(message);
    }


    public void showProgressDialog(int title) {
        ((BaseActivity) getActivity()).onShowProgressDialog(title);
    }


    public void hideProgressDialog() {
        ((BaseActivity) getActivity()).onHideProgressDialog();
    }
}
