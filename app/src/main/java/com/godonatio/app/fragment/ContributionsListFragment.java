package com.godonatio.app.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.godonatio.app.R;
import com.godonatio.app.activity.BaseActivity;
import com.godonatio.app.adapter.ContributionsListAdapter;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.databinding.FragmentContributionsListBinding;
import com.godonatio.app.databinding.LayoutContributionsStatsBinding;
import com.godonatio.app.databinding.LayoutListBinding;
import com.godonatio.app.event.RefreshProjectContributionsEvent;
import com.godonatio.app.event.RefreshProjectEvent;
import com.godonatio.app.model.view.ContributionVM;
import com.godonatio.app.model.view.ProjectVM;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.NetworkUtils;
import com.godonatio.app.util.Utils;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Displays a list of contributions for a project
 */
public class ContributionsListFragment extends ListBindingFragment {


    public static final int SAVED = 33221;
    public static final int UNSAVED = 33222;
    private static final String TAG = "DBG_ContributionsListFragment";
    private static final String ARG_LIST_TYPE = "listType";
    private static final String ARG_PROJECT_JSON = "projectJSON";
    private int mListType;
    private LayoutListBinding mLayoutListBinding;
    private LayoutContributionsStatsBinding mStatsLayoutBinding;
    private ContributionsListAdapter mAdapter;
    private ProjectVM mProjectVM;


    public ContributionsListFragment() {
        // Required empty public constructor
    }


    public static ContributionsListFragment newInstance(String projectJSON, int listType) {
        ContributionsListFragment fragment = new ContributionsListFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_LIST_TYPE, listType);
        args.putString(ARG_PROJECT_JSON, projectJSON);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        mSession = new SessionManager(getContext());
        mListType = getArguments().getInt(ARG_LIST_TYPE, -1);
        mProjectVM = ProjectVM.from(getArguments().getString(ARG_PROJECT_JSON));

        // Bindings
        FragmentContributionsListBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_contributions_list, container, false);
        mLayoutListBinding = binding.layoutList;
        mStatsLayoutBinding = binding.layoutContributionsStats;
        setupListBinding(mLayoutListBinding);

        // override to increaase refresh sensitivity of this list
        mSwipeRefreshContainer.setDistanceToTriggerSync(200);

        View view = binding.getRoot();

        Utils.registerEventBus(this);

        initView(view);

        return view;
    }


    private void initView(View rootView) {
        rootView.setTag(TAG);

        setEmptyText(mLayoutListBinding, getEmptyText());

        mSwipeRefreshContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadContributions(true);
            }
        });

        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadContributions(true);
            }
        });

        // On-load use contributions from json arguments
        loadContributions(false);
    }


    private void loadContributions(boolean retrieveOnline) {
        hideRetryButton(mLayoutListBinding);

        showLoader(mLayoutListBinding);

        if (retrieveOnline)
            makeAPICall(mProjectVM.getId());
        else
            updateView(mProjectVM.getContributions());
    }


    void makeAPICall(long projectId) {
        // make API call to get project details, not just contributions
        if (!NetworkUtils.isConnected(getActivity())) {
            showRetryButton(mLayoutListBinding);
            return;
        }

        APIService.ApiInterface client = APIService.getClient(getContext());
        client.getProject(projectId).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {

                if (!NetworkUtils.isAuthenticated(getContext(), response))
                    getActivity().finish();

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();

                    if (apiResponse.isSuccess()) {
                        mProjectVM = ProjectVM.from(data.getProject());

                        updateView(ContributionVM.from(
                                data.getProject().getContributions(), mProjectVM.getId()));

                        // Always refresh project when list of contributions is refreshed
                        String projectVMJSON = new Gson().toJson(mProjectVM);
                        EventBus.getDefault().post(new RefreshProjectEvent(projectVMJSON, -1));
                    }
                    else if (apiResponse.isError())
                        Log.d(TAG, apiResponse.getData().getError().getMessage());

                    else
                        Log.d(TAG, "apiResponse neither error nor success");
                } else
                    APIUtils.handleRetrofitResponseBodyNull(getActivity(), response);
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                Log.d(TAG, "Error:" + t.toString());

                hideLoader(mLayoutListBinding);

                showRetryButton(mLayoutListBinding);
            }
        });

    }


    void updateView(List<ContributionVM> contributionVMs) {
        hideLoader(mLayoutListBinding);

        mAdapter = new ContributionsListAdapter(contributionVMs);
        mRecyclerView.setAdapter(mAdapter);
        setDatasetSize(mLayoutListBinding, contributionVMs.size());
        mStatsLayoutBinding.setProject(mProjectVM);
    }


    boolean isSavedList() {
        return mListType == SAVED;
    }


    boolean isUnsavedList() {
        return mListType == UNSAVED;
    }


    private String getEmptyText() {
        int resId;
        if (isSavedList()) {
            resId = R.string.empty_text_saved;
        } else if (isUnsavedList()) {
            resId = R.string.empty_text_unsaved;
        } else {
            resId = R.string.empty_text_all;
        }
        return getResources().getString(resId);
    }


    @Subscribe
    public void onEvent(RefreshProjectContributionsEvent event) {
        // Show alert if available
        if (event.getAlertResId() != -1)
            showAlert(event.getAlertResId());

        // Refresh contributions list from online
        makeAPICall(event.getProjectId());
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
