package com.godonatio.app.fragment;


import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.godonatio.app.R;
import com.godonatio.app.databinding.LayoutListBinding;

/**
 * Created by Christian Tamakloe on 16/04/2016.
 */
public class ListBindingFragment extends BaseFragment {


    SwipeRefreshLayout mSwipeRefreshContainer;
    RecyclerView mRecyclerView;
    Button mRetryButton;


    public ListBindingFragment() {
    }


    void setupListBinding(LayoutListBinding binding) {
        mRecyclerView = binding.rvList;
        mSwipeRefreshContainer = binding.swipeRefreshContainer;
        mRetryButton = binding.btnRetry;

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshContainer.setColorSchemeResources(R.color.primary);
        mSwipeRefreshContainer.setDistanceToTriggerSync(300);

//        mSwipeRefreshContainer.setColorSchemeColors(0,0,0,0);
//        mSwipeRefreshContainer.setProgressBackgroundColorSchemeResource(android.R.color.transparent);
    }


    void showRetryButton(LayoutListBinding binding) {
        binding.setShowRetry(true);
    }


    void hideRetryButton(LayoutListBinding binding) {
        binding.setShowRetry(false);
    }


    void showLoader(LayoutListBinding binding) {
        mSwipeRefreshContainer.setRefreshing(true);
    }


    void hideLoader(LayoutListBinding binding) {
        mSwipeRefreshContainer.setRefreshing(false);
    }


    void setDatasetSize(LayoutListBinding binding, int size) {
        binding.setDatasetSize(size);
    }


    void setEmptyText(LayoutListBinding binding, String text) {
        binding.setEmptyText(text);
    }

}
