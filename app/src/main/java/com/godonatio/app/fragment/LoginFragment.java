package com.godonatio.app.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.godonatio.app.R;
import com.godonatio.app.interfaces.ISessionEventListener;
import com.godonatio.app.util.Settings;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private static final String TAG = "DBG_LoginFragment";
    private ISessionEventListener mSessionListener;


    public LoginFragment() {
        // Required empty public constructor
    }


    public static Fragment newInstance() {
        return new LoginFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mSessionListener = (ISessionEventListener) getActivity();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        return view;
    }


    @OnClick(R.id.rl_register_link)
    void onRegisterLinkClick() {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.session_fragment_container,
                        RegisterFragment.newInstance(),
                        TAG)
                .commit();
    }


    @OnClick(R.id.btn_login)
    void onLoginButtonClick() {
        loginUser();
    }


    private void loginUser() {
        mSessionListener.onProcessSessionRequest(ISessionEventListener.LOGIN);
    }
}
