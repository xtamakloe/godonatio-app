package com.godonatio.app.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.godonatio.app.R;
import com.godonatio.app.activity.BaseActivity;
import com.godonatio.app.activity.CreateContributionActivity;
import com.godonatio.app.activity.CreateUpdateProjectActivity;
import com.godonatio.app.activity.ProjectActivity;
import com.godonatio.app.adapter.ContributionsPagerAdapter;
import com.godonatio.app.adapter.ProjectInfoPagerAdapter;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.event.RefreshProjectEvent;
import com.godonatio.app.event.RefreshProjectsListEvent;
import com.godonatio.app.model.view.ProjectVM;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.NetworkUtils;
import com.godonatio.app.util.UIUtils;
import com.godonatio.app.util.Utils;
import com.viewpagerindicator.CirclePageIndicator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectFragment extends BaseFragment {

    public static final String TAG = "DBG_ProjectFragment";
    public static final String KEY_PROJECT_JSON = "mProjectJSON";
    private static final String ARG_PROJECT_JSON = "mProjectJSON";
    private static final String ARG_ALERT = "alert";
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.vp_project_info_pages)
    ViewPager mProjectInfoViewPager;
    @Bind(R.id.vp_contribution_pages)
    ViewPager mContributionsViewPager;
    @Bind(R.id.page_indicator)
    CirclePageIndicator mPageIndicator;
    private String mProjectJSON;
    private ProjectVM mProjectVM;
    private BaseActivity mActivity;


    public ProjectFragment() {
        // Required empty public constructor
    }


    public static ProjectFragment newInstance(String projectJSON, String alert) {
        ProjectFragment fragment = new ProjectFragment();

        Bundle args = new Bundle();
        args.putString(ARG_PROJECT_JSON, projectJSON);
        args.putString(ARG_ALERT, alert);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project, container, false);

        ButterKnife.bind(this, view);

        Utils.registerEventBus(this);

        mActivity = (BaseActivity) getActivity();

        mActivity.setSupportActionBar(mToolbar);

        initView();

        return view;
    }


    void initView() {
        String alert = getArguments().getString(ARG_ALERT);
        mProjectJSON = getArguments().getString(ARG_PROJECT_JSON);
        mProjectVM = ProjectVM.from(mProjectJSON);
        FragmentManager supportFragmentManager = mActivity.getSupportFragmentManager();

        mActivity.setSupportActionBar(mToolbar);
        mToolbar.setTitle(mProjectVM.getName());
        UIUtils.setHomeAsUpIndicator(mToolbar, mActivity);

        setHasOptionsMenu(true);

        if (alert != null)
            mActivity.showAlert(alert);

        // Project details view
        ProjectInfoPagerAdapter mProjectInfoPagerAdapter =
                new ProjectInfoPagerAdapter(supportFragmentManager, mProjectJSON);
        mProjectInfoViewPager.setAdapter(mProjectInfoPagerAdapter);
        mPageIndicator.setViewPager(mProjectInfoViewPager);

        mPageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }


            @Override
            public void onPageSelected(int position) {
                int color;
                if (position == 1)
                    color = R.color.primary_light;
                else
                    color = R.color.primary;

                mPageIndicator.setBackgroundColor(ContextCompat.getColor(getActivity(), color));
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /*mProjectInfoViewPager.setOffscreenPageLimit(2);
        mIndicator.setViewPager(mProjectInfoViewPager);*/

        // Project contributions list view
        ContributionsPagerAdapter mContributionsPagerAdapter =
                new ContributionsPagerAdapter(supportFragmentManager, mProjectJSON);
        mContributionsViewPager.setAdapter(mContributionsPagerAdapter);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_project, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_project:
                Intent updateIntent =
                        new Intent(getContext(), CreateUpdateProjectActivity.class);
                updateIntent.putExtra(ProjectActivity.VM_JSON, mProjectJSON);
                startActivity(updateIntent);
                return true;
            case R.id.action_delete_project:
                showDeleteProjectDialog();

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.fab)
    void onFABClick() {
        // Allow donations for only active projects
        if (mProjectVM.isActive()) {
            Intent intent = new Intent(getContext(), CreateContributionActivity.class);
            intent.putExtra(KEY_PROJECT_JSON, mProjectJSON);
            startActivity(intent);
        } else
            showAlert(R.string.error_project_inactive);
    }


    @Subscribe
    public void onEvent(RefreshProjectEvent event) {
        // Update project object and json
        if (event.getProjectJSON() != null) {
            mProjectJSON = event.getProjectJSON();
            mProjectVM = ProjectVM.from(mProjectJSON);
        }
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    private void showDeleteProjectDialog() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.confirm_delete_project_dialog_title)
                .content(R.string.confirm_delete_project_dialog_content)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();

                        deleteProject();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    private void deleteProject() {
        showProgressDialog(R.string.deleting_project);

        if (!NetworkUtils.isConnected(getActivity()))
            return;

        Log.d(TAG, "projectId is: " + String.valueOf(mProjectVM.getId()));

        APIService.ApiInterface client = APIService.getClient(getActivity());

        client.deleteProject(mProjectVM.getId()).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                hideProgressDialog(); // Dismiss progress dialog

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();
                    // if successful
                    if (apiResponse.isSuccess()) {
                        // Refresh project list
                        EventBus.getDefault().post(
                                new RefreshProjectsListEvent(
                                        mProjectVM.getName() + " deleted successfully"
                                ));

                        getActivity().finish();

                    } else if (apiResponse.isError()) {
                        String errorMsg = getResources().getString(
                                R.string.deleting_project_failed);
                        showAlert(errorMsg);
                    }
                } else
                    APIUtils.handleRetrofitResponseBodyNull(getActivity(), response);
            }


            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                hideProgressDialog();

                APIUtils.handleRetrofitFailure(getActivity(), t);
            }
        });
    }
}

