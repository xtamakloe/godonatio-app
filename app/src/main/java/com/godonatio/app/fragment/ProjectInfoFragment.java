package com.godonatio.app.fragment;


import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

import com.afollestad.materialdialogs.MaterialDialog;
import com.godonatio.app.R;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.databinding.FragmentProjectInfoProjectDetailsBinding;
import com.godonatio.app.databinding.FragmentProjectInfoProjectSummaryBinding;
import com.godonatio.app.event.RefreshProjectEvent;
import com.godonatio.app.event.RefreshProjectsListEvent;
import com.godonatio.app.model.view.ProjectVM;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.NetworkUtils;
import com.godonatio.app.util.Utils;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectInfoFragment extends BaseFragment {

    public static final int PROJECT_DETAILS = 233223;
    public static final int PROJECT_SUMMARY = 2211223;
    private static final String TAG = "DBG_ProjectInfoFragment";
    private static final String ARG_INFO_TYPE = "infoType";
    private static final String ARG_PROJECT_JSON = "projectJSON";

    private int mInfoType;
    private ProjectVM mProjectVM;
    private ViewDataBinding mBinding;
    private ToggleButton mToggleButton;


    public ProjectInfoFragment() {
        // Required empty public constructor
    }


    public static ProjectInfoFragment newInstance(int infoType, String projectJSON) {
        ProjectInfoFragment fragment = new ProjectInfoFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_INFO_TYPE, infoType);
        args.putString(ARG_PROJECT_JSON, projectJSON);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // mProject = get project here, from JSON probably, save it
        // project info: from JSON
        // contribution info: from online

        mInfoType = getArguments().getInt(ARG_INFO_TYPE, -1);
        String projectJSON = getArguments().getString(ARG_PROJECT_JSON, null);

        int layout = -1;
        if (isProjectSummary())
            layout = R.layout.fragment_project_info_project_summary;

        else if (isProjectDetails())
            layout = R.layout.fragment_project_info_project_details;

        mBinding = DataBindingUtil.inflate(inflater, layout, container, false);

        View view = mBinding.getRoot();
        view.setTag(TAG);

        ButterKnife.bind(this, view);

        Utils.registerEventBus(this);

        initView(projectJSON);

        return view;
    }


    private void initView(String projectJSON) {
        mProjectVM = new Gson().fromJson(projectJSON, ProjectVM.class);

        if (isProjectSummary())
            initProjectSummaryView(mProjectVM);

        else if (isProjectDetails())
            initProjectDetailsView(mProjectVM);
    }


    private void initProjectSummaryView(ProjectVM projectVM) {
        FragmentProjectInfoProjectSummaryBinding projectSummaryBinding =
                (FragmentProjectInfoProjectSummaryBinding) mBinding;
        projectSummaryBinding.setProject(projectVM);
    }


    private void initProjectDetailsView(ProjectVM projectVM) {
        final FragmentProjectInfoProjectDetailsBinding projectDetailsBinding =
                (FragmentProjectInfoProjectDetailsBinding) mBinding;
        projectDetailsBinding.setProject(projectVM);

        projectDetailsBinding.btnTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddVoucherDialog();
            }
        });

        // binding can go into bindinghandler
        // need a dialog to show whether project was made active or not
        mToggleButton = projectDetailsBinding.tbToggleProjectStatus;
        mToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleProjectStatus(mToggleButton.isChecked());
            }
        });
    }


    private boolean isProjectSummary() {
        return mInfoType == PROJECT_SUMMARY;
    }


    private boolean isProjectDetails() {
        return mInfoType == PROJECT_DETAILS;
    }


    @Subscribe
    public void onEvent(RefreshProjectEvent event) {
        // Show alert if available
        if (event.getAlertResId() != -1)
            showAlert(event.getAlertResId());

        // Refresh view
        if (event.getProjectJSON() != null)
            initView(event.getProjectJSON());

        // always refresh list of projects when a project is refreshed
        EventBus.getDefault().post(new RefreshProjectsListEvent(null));
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    private void toggleProjectStatus(boolean status) {
        showProgressDialog(R.string.updating_project_status);

        if (!NetworkUtils.isConnected(getActivity()))
            return;

        APIService.ApiInterface client = APIService.getClient(getContext());
        client.toggleProjectStatus(mProjectVM.getId(), status ? "1" : "0")
                .enqueue(new Callback<APIResponse>() {
                    @Override
                    public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                        hideProgressDialog();

                        if (response.body() != null) {
                            APIResponse apiResponse = response.body();
                            APIResponse.Data data = apiResponse.getData();

                            if (apiResponse.isSuccess()) {
                                // Refresh project details with fresh info
                                String projectVMJSON = new Gson().toJson(
                                        ProjectVM.from(data.getProject()));

                                int alertResId = R.string.project_status_updated;

                                EventBus.getDefault().post(
                                        new RefreshProjectEvent(projectVMJSON, alertResId));

                            } else if (apiResponse.isError())
                                showAlert(data.getError().getMessage());
                        } else
                            APIUtils.handleRetrofitResponseBodyNull(getActivity(), response);
                    }


                    @Override
                    public void onFailure(Call<APIResponse> call, Throwable t) {
                        hideProgressDialog();

                        APIUtils.handleRetrofitFailure(getActivity(), t);
                    }
                });
    }


    void showAddVoucherDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.add_voucher_dialog_title)
                .content(R.string.add_voucher_dialog_content)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(R.string.add_voucher_dialog_input_hint,
                        R.string.add_voucher_dialog_input_prefill,
                        new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                if (!input.toString().trim().isEmpty())
                                    performVoucherAddition(input.toString());
                                else {
                                    showAlert(R.string.add_voucher_dialog_input_error);

                                    /*showAddVoucherDialog();*/
                                }
                            }
                        })
                .negativeText(R.string.cancel)
                .show();
    }


    void performVoucherAddition(String voucherNumber) {
        showProgressDialog(R.string.adding_voucher);

        if (!NetworkUtils.isConnected(getActivity()))
            return;

        APIService.ApiInterface client = APIService.getClient(getContext());
        client.createTopUp(mProjectVM.getId(), voucherNumber).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                hideProgressDialog();

                if (!NetworkUtils.isAuthenticated(getContext(), response))
                    getActivity().finish();

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();
                    // if successful
                    if (apiResponse.isSuccess()) {
                        // Refresh project details
                        String projectVMJSON = new Gson().toJson(ProjectVM.from(data.getProject()));
                        EventBus.getDefault().post(
                                new RefreshProjectEvent(projectVMJSON,
                                        R.string.adding_voucher_successful)
                        );
                    } else if (apiResponse.isError()) {
                        String errorMsg = getResources().getString(
                                R.string.adding_voucher_failed)
                                + " "
                                + apiResponse.getData().getError().getMessage();
                        showAlert(errorMsg);
                    } else
                        Log.d(TAG, "apiResponse else");

                } else {
                    APIUtils.handleRetrofitResponseBodyNull(getActivity(), response);
                }
            }


            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                APIUtils.handleRetrofitFailure(getActivity(), t);
            }
        });
    }
}
