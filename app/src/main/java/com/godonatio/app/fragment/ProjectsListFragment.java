package com.godonatio.app.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.godonatio.app.R;
import com.godonatio.app.activity.CreateContributionActivity;
import com.godonatio.app.adapter.ProjectsListAdapter;
import com.godonatio.app.api.APIResponse;
import com.godonatio.app.api.APIService;
import com.godonatio.app.databinding.FragmentProjectsListBinding;
import com.godonatio.app.databinding.LayoutListBinding;
import com.godonatio.app.event.RefreshProjectsListEvent;
import com.godonatio.app.model.view.ProjectVM;
import com.godonatio.app.util.APIUtils;
import com.godonatio.app.util.NetworkUtils;
import com.godonatio.app.util.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectsListFragment extends ListBindingFragment {

    public static final int EVENTS = 1231;
    public static final int CAUSES = 1232;
    public static final int GROUPS = 1233;
    public static final int PAYMENTS = 1234;
    public static final String TAG = "DBG_ProjectsListFragment";
    public static final String KEY_PROJECT_NAMES = "projectNames";
    public static final String KEY_PROJECT_IDS = "projectIds";
    private static final String ARG_LIST_TYPE = "listType";
    private ProjectsListAdapter mAdapter;
    private int mListType;
    private LayoutListBinding mLayoutListBinding;
    private List<ProjectVM> mProjectVMs;


    public ProjectsListFragment() {
        // Required empty public constructor
    }


    public static ProjectsListFragment newInstance(int listType) {
        ProjectsListFragment fragment = new ProjectsListFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_LIST_TYPE, listType);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mListType = getArguments().getInt(ARG_LIST_TYPE, -1);

        FragmentProjectsListBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_projects_list, container, false);
        mLayoutListBinding = binding.layoutList;
        setupListBinding(mLayoutListBinding);

        View view = binding.getRoot();

        Utils.registerEventBus(this);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSwipeRefreshContainer.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshContainer.setRefreshing(true);

                initView();
            }
        });
    }


    private void initView() {
        mSwipeRefreshContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadProjects(true);
            }
        });

        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadProjects(true);
            }
        });

        loadProjects(true);
    }


    void loadProjects(boolean retrieveOnline) {
        hideRetryButton(mLayoutListBinding);

        showLoader(mLayoutListBinding);

        if (retrieveOnline)
            makeAPICall();
        else
            updateView(mProjectVMs); // TODO: find a way to load projects into fragment
    }


    void updateView(List<ProjectVM> projectVMs) {
        hideLoader(mLayoutListBinding);

        mProjectVMs = projectVMs;
        mAdapter = new ProjectsListAdapter(projectVMs);
        mRecyclerView.setAdapter(mAdapter);
        setDatasetSize(mLayoutListBinding, projectVMs.size());
        if (projectVMs.size() == 0)
            setEmptyText(mLayoutListBinding, getEmptyText());
    }


    public void onFABClick() {
        if (mProjectVMs == null)
            return;

        if (!(mProjectVMs.size() > 0)) {
            showAlert(getResources().getString(R.string.add_contribution_fab_error));
            return;
        }

        ArrayList<String> projectNames = new ArrayList<>();
        ArrayList<Long> projectIds = new ArrayList<>();

        // Collect only active projects for display
        for (ProjectVM project : mProjectVMs) {
            if (project.isActive()) {
                projectNames.add(project.getName());
                projectIds.add(project.getId());
            }
        }

        if (projectNames.size() > 0) {
            Intent intent = new Intent(getContext(), CreateContributionActivity.class);
            intent.putStringArrayListExtra(KEY_PROJECT_NAMES, projectNames);
            intent.putExtra(KEY_PROJECT_IDS, projectIds);
            startActivity(intent);
        } else
            showAlert(R.string.error_no_active_projects);
    }


    private void makeAPICall() {
        if (!NetworkUtils.isConnected(getActivity())) {
            showRetryButton(mLayoutListBinding);
            return;
        }

        APIService.ApiInterface client = APIService.getClient(getContext());
        Call<APIResponse> call;
        // Do i really need to make 4 different requests?
        // events
        if (isEventsList())
            call = client.getUserEventProjects();
            // causes
        else if (isCausesList())
            call = client.getUserCauseProjects();
            // groups
        else if (isGroupsList())
            call = client.getUserGroupProjects();
            // payments
        else if (isPaymentsList())
            call = client.getUserPaymentProjects();
            // anything else
        else
            call = client.getUserProjects();

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {

                if (!NetworkUtils.isAuthenticated(getContext(), response))
                    getActivity().finish();

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();

                    if (apiResponse.isSuccess())
                        updateView(ProjectVM.from(data.getProjects()));

                    else if (apiResponse.isError())
                        Log.d(TAG, apiResponse.getData().getError().getMessage());

                    else
                        Log.d(TAG, "not error not success");

                } else
                    APIUtils.handleRetrofitResponseBodyNull(getActivity(), response);
            }


            @SuppressLint("LongLogTag")
            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                Log.d(TAG, "Error:" + t.toString());

                hideLoader(mLayoutListBinding);

                showRetryButton(mLayoutListBinding);
            }
        });
    }


    boolean isEventsList() {
        return mListType == EVENTS;
    }


    boolean isCausesList() {
        return mListType == CAUSES;
    }


    boolean isGroupsList() {
        return mListType == GROUPS;
    }


    boolean isPaymentsList() {
        return mListType == PAYMENTS;
    }


    private String getEmptyText() {
        int resId;
        if (isEventsList()) {
            resId = R.string.empty_text_events;
        } else if (isCausesList()) {
            resId = R.string.empty_text_causes;
        } else if (isGroupsList()) {
            resId = R.string.empty_text_groups;
        } else if (isPaymentsList()) {
            resId = R.string.empty_text_payments;
        } else {
            resId = R.string.empty_text_all;
        }
        return getResources().getString(resId);
    }


    @Subscribe
    public void onEvent(RefreshProjectsListEvent event) {
        // Show alert if available
        if (event.getAlertMsg() != null)
            showAlert(event.getAlertMsg());

        makeAPICall();
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
