package com.godonatio.app.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.godonatio.app.R;
import com.godonatio.app.interfaces.ISessionEventListener;
import com.godonatio.app.interfaces.IUIEventListener;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    private static final String TAG = "DBG_RegisterFragment";
    private ISessionEventListener mSessionListener;


    public RegisterFragment() {
        // Required empty public constructor
    }


    public static Fragment newInstance() {
        return new RegisterFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mSessionListener = (ISessionEventListener) getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);

        return view;
    }


    @OnClick(R.id.rl_login_link)
    void onLoginLinkClick() {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.session_fragment_container,
                        LoginFragment.newInstance(),
                        TAG)
                .commit();
    }


    @OnClick(R.id.btn_register)
    void onRegisterButtonClick() {
        registerUser();
    }


    private void registerUser() {
        mSessionListener.onProcessSessionRequest(ISessionEventListener.REGISTER);
    }
}
