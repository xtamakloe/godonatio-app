package com.godonatio.app.interfaces;

/**
 * Created by Christian Tamakloe on 04/04/2016.
 */
public interface ISessionEventListener {
    public static final int LOGIN = 4950;
    public static final int REGISTER = 4955;

    void onProcessSessionRequest(int sessionMode);
}
