package com.godonatio.app.interfaces;


/**
 * Created by Christian Tamakloe on 04/04/2016.
 */
public interface IUIEventListener {
    public void onSnackBar(String message);

    public void onShowInfoDialog(String title, String content);

    public void onShowProgressDialog(int titleResId);

    public void onHideProgressDialog();
}
