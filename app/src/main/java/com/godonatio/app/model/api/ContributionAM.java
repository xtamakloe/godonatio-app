package com.godonatio.app.model.api;


/**
 * Created by Christian Tamakloe on 13/04/2016.
 */
public class ContributionAM {
    private static final String TAG = "DBG_ContributionAM";
    private Long id;
    private String contribution_type;
    private String amount;
    private String currency_symbol;
    private String signature;
    private String remarks;
    private String contributor_name;
    private String contributor_msisdn;
    private String received_at;
    private String pic_url_thumb;
    private String pic_url_original;


    public Long getId() {
        return id;
    }


    public String getContributionType() {
        return contribution_type;
    }


    public String getAmount() {
        return amount;
    }


    public String getSignature() {
        return signature;
    }


    public String getRemarks() {
        return remarks;
    }


    public String getContributorName() {
        return contributor_name;
    }


    public String getContributorMsisdn() {
        return contributor_msisdn;
    }


    public String getCurrencySymbol() {
        return currency_symbol;
    }


    public String getReceivedAt() {
        return received_at;
    }


    public String getPicUrlThumb() {
        if (pic_url_thumb.equalsIgnoreCase("/images/placeholder.png"))
            return null;

        return pic_url_thumb;
    }


    public String getPicUrlOriginal() {
        if (pic_url_original.equalsIgnoreCase("/images/placeholder.png"))
            return null;

        return pic_url_original;
    }
}
