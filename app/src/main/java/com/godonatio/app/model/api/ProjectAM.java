package com.godonatio.app.model.api;


import java.util.List;

/**
 * Created by Christian Tamakloe on 13/04/2016.
 */
public class ProjectAM {
    private long id;
    private String name;
    private String description;
    private String project_type;
    private boolean active;
    private String cash_amount;
    private String cheque_amount;
    private int gift_count;
    private String default_currency;
    private String available_credits;
    private List<ContributionAM> contributions;


    public String getAvailableCredits() {
        return available_credits;
    }


    public String getCashAmount() {
        return cash_amount;
    }


    public String getChequeAmount() {
        return cheque_amount;
    }


    public int getGiftCount() {
        return gift_count;
    }


    public String getDefaultCurrency() {
        return default_currency;
    }


    public long getId() {
        return id;
    }


    public String getName() {
        return name;
    }


    public String getDescription() {
        return description;
    }


    public String getProjectType() {
        return project_type;
    }


    public List<ContributionAM> getContributions() {
        return contributions;
    }


    public boolean isActive() {
        return active;
    }
}
