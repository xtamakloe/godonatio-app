package com.godonatio.app.model.api;

/**
 * Created by Christian Tamakloe on 11/04/2016.
 */
public class UserAM {
    private long id;
    private String first_name;
    private String last_name;
    private String email;

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }
}
