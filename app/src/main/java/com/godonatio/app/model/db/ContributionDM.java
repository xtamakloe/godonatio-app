package com.godonatio.app.model.db;

/**
 * Created by Christian Tamakloe on 21/04/2016.
 */
public class ContributionDM {
    private Long id;
    private String contributionType;
    private Float amount;
    private String signature;
    private String remarks;
    private String contributorName;
    private String contributorPhoneNo;
    private boolean synced;
    private long createdAt;
    private String currency;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContributionType() {
        return contributionType;
    }

    public void setContributionType(String contributionType) {
        this.contributionType = contributionType;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    public String getContributorPhoneNo() {
        return contributorPhoneNo;
    }

    public void setContributorPhoneNo(String contributorPhoneNo) {
        this.contributorPhoneNo = contributorPhoneNo;
    }
}
