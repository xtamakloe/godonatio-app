package com.godonatio.app.model.view;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.godonatio.app.BR;
import com.godonatio.app.model.api.ContributionAM;
import com.godonatio.app.util.UIUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian Tamakloe on 13/04/2016.
 */
public class ContributionVM extends BaseObservable {
    public final static String TYPE_GIFT = "gift";
    public final static String TYPE_CASH = "cash";
    public final static String TYPE_CHEQUE = "cheque";
    private static final String TAG = "DBG_ContributionVM";
    private Long id;
    private String contributionType;
    private String amount;
    private String currencySymbol;
    private String signature;
    private String remarks;
    private String contributorName;
    private String contributorMsisdn;
    private String receivedAt;
    private String picUrlThumb;
    private String picUrlOriginal;
    private long projectId;


    public static ContributionVM from(ContributionAM contributionAM, long projectId) {
        ContributionVM contributionVM = new ContributionVM();

        contributionVM.setId(contributionAM.getId());
        contributionVM.setContributionType(contributionAM.getContributionType());
        contributionVM.setAmount(contributionAM.getAmount());
        contributionVM.setCurrencySymbol(contributionAM.getCurrencySymbol());
        contributionVM.setSignature(contributionAM.getSignature());
        contributionVM.setRemarks(contributionAM.getRemarks());
        contributionVM.setContributorName(contributionAM.getContributorName());
        contributionVM.setContributorMsisdn(contributionAM.getContributorMsisdn());
        contributionVM.setReceivedAt(contributionAM.getReceivedAt());
        contributionVM.setPicUrlThumb(contributionAM.getPicUrlThumb());
        contributionVM.setPicUrlOriginal(contributionAM.getPicUrlOriginal());
        contributionVM.setProjectId(projectId);

        return contributionVM;
    }


    /**
     * Convert group of {@link ContributionAM} models to {@link ContributionVM} models
     */
    public static List<ContributionVM> from(List<ContributionAM> contributionAMs, long projectId) {
        List<ContributionVM> contributionVMs = new ArrayList<>();
        for (ContributionAM contributionAM : contributionAMs) {
            contributionVMs.add(from(contributionAM, projectId));
        }
        return contributionVMs;
    }


    @Bindable
    public String getPicUrlThumb() {
        return picUrlThumb;
    }


    public void setPicUrlThumb(String picUrlThumb) {
        this.picUrlThumb = picUrlThumb;
        notifyPropertyChanged(BR.picUrlThumb);
    }


    @Bindable
    public String getPicUrlOriginal() {
        return picUrlOriginal;
    }


    public void setPicUrlOriginal(String picUrlOriginal) {
        this.picUrlOriginal = picUrlOriginal;
        notifyPropertyChanged(BR.picUrlOriginal);
    }


    public boolean hasGiftImage() {
        return this.picUrlOriginal != null && this.picUrlThumb != null;
    }


    @Bindable
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    @Bindable
    public String getContributionType() {
        return contributionType;
    }


    public void setContributionType(String contributionType) {
        this.contributionType = contributionType;
        notifyPropertyChanged(BR.contributionType);
    }


    @Bindable
    public String getAmount() {
        return UIUtils.formatMoney(getCurrencySymbol(), amount);
    }


    public void setAmount(String amount) {
        this.amount = amount;
        notifyPropertyChanged(BR.amount);
    }


    @Bindable
    public String getSignature() {
        return signature;
    }


    public void setSignature(String signature) {
        this.signature = signature;
        notifyPropertyChanged(BR.signature);
    }


    @Bindable
    public String getRemarks() {
        return remarks;
    }


    public void setRemarks(String remarks) {
        this.remarks = remarks;
        notifyPropertyChanged(BR.remarks);
    }


    @Bindable
    public String getContributorName() {
        return contributorName;
    }


    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
        notifyPropertyChanged(BR.contributorName);
    }


    @Bindable
    public String getContributorMsisdn() {
        return contributorMsisdn;
    }


    public void setContributorMsisdn(String contributorMsisdn) {
        this.contributorMsisdn = contributorMsisdn;
        notifyPropertyChanged(BR.contributorMsisdn);
    }


    public String getTypeIcon() {
        switch (getContributionType()) {
            case TYPE_GIFT:
                return "{faw-gift}";

            case TYPE_CASH:
                return "{faw-money}";

            case TYPE_CHEQUE:
//                return "{gmd-credit-card}";
                return "{faw-pencil-square-o}";

            default:
                return "";
        }
    }


    public boolean isGift() {
        return getContributionType().equals(TYPE_GIFT);
    }


    public boolean isCash() {
        return getContributionType().equals(TYPE_CASH);
    }


    public boolean isCheque() {
        return getContributionType().equals(TYPE_CHEQUE);
    }


    @Bindable
    public String getCurrencySymbol() {
        return currencySymbol;
    }


    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
        notifyPropertyChanged(BR.currencySymbol);
    }


    @Bindable
    public String getReceivedAt() {
        if (receivedAt == null)
            return "";

        return UIUtils.formatDate(receivedAt);
    }


    public void setReceivedAt(String receivedAt) {
        this.receivedAt = receivedAt;
        notifyPropertyChanged(BR.receivedAt);
    }


    public long getProjectId() {
        return projectId;
    }


    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }
}
