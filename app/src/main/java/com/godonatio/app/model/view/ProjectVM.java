package com.godonatio.app.model.view;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.godonatio.app.BR;
import com.godonatio.app.model.api.ContributionAM;
import com.godonatio.app.model.api.ProjectAM;
import com.godonatio.app.util.UIUtils;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian Tamakloe on 13/04/2016.
 */
public class ProjectVM extends BaseObservable {
    private final DecimalFormat mFormatter;
    private long id;
    private String name;
    private String description;
    private String projectType;
    private boolean active;
    private List<ContributionVM> contributionVMs = new ArrayList<>();
    private String cashAmount;
    private String chequeAmount;
    private int giftCount;
    private String defaultCurrency;
    private String availableCredits;


    public ProjectVM() {
        mFormatter = new DecimalFormat("###,###.##");
        mFormatter.setMinimumFractionDigits(2);
    }


    /**
     * Converts a {@link ProjectAM} model to {@link ProjectVM} model
     *
     * @param projectAM
     * @return
     */
    public static ProjectVM from(ProjectAM projectAM) {
        ProjectVM projectVM = new ProjectVM();
        projectVM.setId(projectAM.getId());
        projectVM.setName(projectAM.getName());
        projectVM.setDescription(projectAM.getDescription());
        projectVM.setProjectType(projectAM.getProjectType());
        projectVM.setActive(projectAM.isActive());
        projectVM.setCashAmount(projectAM.getCashAmount());
        projectVM.setChequeAmount(projectAM.getChequeAmount());
        projectVM.setGiftCount(projectAM.getGiftCount());
        projectVM.setDefaultCurrency(projectAM.getDefaultCurrency());
        projectVM.setAvailableCredits(projectAM.getAvailableCredits());

        // Convert api contribution models to view models
        List<ContributionVM> contributionVMs = new ArrayList<ContributionVM>();
        for (ContributionAM contributionAM : projectAM.getContributions()) {
            contributionVMs.add(ContributionVM.from(contributionAM, projectAM.getId()));
        }
        projectVM.setContributions(contributionVMs);

        return projectVM;
    }


    /**
     * Convert group of {@link ProjectAM} models to {@link ProjectVM} models
     */
    public static List<ProjectVM> from(List<ProjectAM> projectAMs) {
        List<ProjectVM> projectVMs = new ArrayList<>();
        for (ProjectAM projectAM : projectAMs) {
            projectVMs.add(from(projectAM));
        }
        return projectVMs;
    }


    public static ProjectVM from(String projectJSON) {
        return new Gson().fromJson(projectJSON, ProjectVM.class);
    }


    @Bindable
    public String getCashAmount() {
        return UIUtils.formatMoney("", cashAmount);
    }


    public void setCashAmount(String cashAmount) {
        this.cashAmount = cashAmount;
        notifyPropertyChanged(BR.cashAmount);
    }


    @Bindable
    public String getChequeAmount() {
        return UIUtils.formatMoney("", chequeAmount);
    }


    public void setChequeAmount(String chequeAmount) {
        this.chequeAmount = chequeAmount;
        notifyPropertyChanged(BR.chequeAmount);
    }


    @Bindable
    public String getGiftCount() {
//        return mFormatter.format(giftCount);
        return UIUtils.formatMoney("", Integer.toString(giftCount));
    }


    public void setGiftCount(int giftCount) {
        this.giftCount = giftCount;
        notifyPropertyChanged(BR.giftCount);
    }


    @Bindable
    public String getDefaultCurrency() {
        return defaultCurrency;
    }


    public void setDefaultCurrency(String defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
        notifyPropertyChanged(BR.defaultCurrency);
    }


    /* Getters and Setters */
    @Bindable
    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }


    @Bindable
    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }


    @Bindable
    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }


    @Bindable
    public String getProjectType() {
        return projectType;
    }


    public void setProjectType(String projectType) {
        this.projectType = projectType;
        notifyPropertyChanged(BR.projectType);
    }


    public List<ContributionVM> getContributions() {
        return contributionVMs;
    }


    public void setContributions(List<ContributionVM> contributions) {
        this.contributionVMs = contributions;
    }


    @Bindable
    public boolean isActive() {
        return active;
    }


    public void setActive(boolean active) {
        this.active = active;
        notifyPropertyChanged(BR.active);
    }


    @Bindable
    public String getAvailableCredits() {
        return availableCredits;
    }


    public void setAvailableCredits(String availableCredits) {
        this.availableCredits = availableCredits;
        notifyPropertyChanged(BR.availableCredits);
    }


    public boolean hasContributions() {
        return getContributions().size() != 0;
    }


    public boolean hasGifts() {
        return !(giftCount == 0);
    }


    public boolean hasCash() {
        return !cashAmount.equals("0.00");
    }


    public boolean hasCheques() {
        return !chequeAmount.equals("0.00");
    }
}
