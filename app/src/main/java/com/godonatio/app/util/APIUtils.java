package com.godonatio.app.util;


import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.godonatio.app.R;
import com.godonatio.app.api.APIResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Response;

/**
 * Created by Christian Tamakloe on 11/04/2016.
 */
public class APIUtils {

    private static final String TAG = "DBG_APIUtils";


    public static void handleRetrofitResponseBodyNull(Activity activity, Response response) {
        try {
            String errorJson = response.errorBody().string();
            APIResponse errorResponse =
                    new Gson().fromJson(errorJson, APIResponse.class);

            int code = errorResponse.getData().getError().getCode();
            String message = errorResponse.getData().getError().getMessage();

            if (code == 401) // Auth error doesn't return body
                new SessionManager(activity).logout();

            Log.d(TAG, "message: " + message);

            showAlert(activity, message);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (java.lang.IllegalStateException e) {
            e.printStackTrace();
            Log.d(TAG, "Error converting JSON maybe");
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Server error");
        } finally {
            showAlert(activity, "Something went wrong. Please try again in a moment.");
        }
    }


    public static void handleRetrofitFailure(Activity activity, Throwable t) {

        String exceptionMsg = null;
        try {
            Log.d(TAG, "Retrofit onFailure: " + t.toString());
            throw t;
        } catch (SocketTimeoutException e) {
            exceptionMsg = activity.getResources().getString(R.string.socket_timeout_error);
            e.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            exceptionMsg = activity.getResources().getString(R.string.general_connection_error);
        } finally {
            showAlert(activity, exceptionMsg);
        }
    }


    private static void showAlert(Activity activity, String errorMsg) {
        Snackbar.make(activity.findViewById(android.R.id.content),
                errorMsg,
                Snackbar.LENGTH_LONG).show();
    }
}
