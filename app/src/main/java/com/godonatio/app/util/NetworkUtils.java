package com.godonatio.app.util;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;

import com.godonatio.app.R;
import com.godonatio.app.api.APIResponse;

import retrofit2.Response;

/**
 * Created by Christian Tamakloe on 29/04/2016.
 */
public class NetworkUtils {
    public static boolean isConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (!isConnected) {
            Snackbar.make(activity.findViewById(android.R.id.content),
                    R.string.network_error, Snackbar.LENGTH_LONG).show();
        }
        return isConnected;
    }


    public static boolean isAuthenticated(Context context, Response<APIResponse> response) {
        if (response.code() == 401) {
            new SessionManager(context).logout();
            return false;
        }
        return true;
    }
}
