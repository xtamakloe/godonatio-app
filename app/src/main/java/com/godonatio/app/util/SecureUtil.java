package com.godonatio.app.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Created by Christian Tamakloe on 21/04/2016.
 */
public class SecureUtil {
    /*public static String generateHash(String passwordToHash, String salt) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(salt.getBytes());
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    public static boolean authenticateAgent(Agent agent, String pin) {
        return generateHash(pin, agent.getAuthToken()).equals(agent.getPinDigest());
    }*/

    public static String generateUniqueString() {
        return new BigInteger(130, new SecureRandom()).toString(32);
    }
}
