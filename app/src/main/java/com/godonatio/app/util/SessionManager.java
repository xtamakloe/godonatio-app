package com.godonatio.app.util;


import android.content.Context;

import com.godonatio.app.model.api.SessionAM;

/**
 * Created by Christian Tamakloe on 11/04/2016.
 */
public class SessionManager {

    private final Context mContext;


    public SessionManager(Context context) {
        mContext = context;
    }


    public boolean isValid() {
        if (Settings.getInstance(mContext).isUserLoggedIn())
            return true;

        gotoLoginScreen();
        return false;
    }


    public void login(String uid, String accessToken, String clientId, SessionAM session) {
        Settings.getInstance(mContext).createSession(uid, accessToken, clientId, session);
    }


    public void logout() {
        Settings.getInstance(mContext).destroySession();

        gotoLoginScreen();
    }


    public void gotoLoginScreen() {
        Settings.getInstance(mContext).gotoLoginScreen();
    }


    public String getToken() {
        return Settings.getInstance(mContext).getAccessToken();
    }


    public String getUid() {
        return Settings.getInstance(mContext).getUserUid();
    }


    public String getClientId() {
        return Settings.getInstance(mContext).getClientId();
    }
}
