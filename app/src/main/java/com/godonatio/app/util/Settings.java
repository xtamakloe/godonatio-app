package com.godonatio.app.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.godonatio.app.activity.SessionActivity;
import com.godonatio.app.model.api.SessionAM;

/**
 * Created by Christian Tamakloe on 04/04/2016.
 */

public class Settings {

    private static final String TAG = "DBG_Settings";
    private static final String KEY_API_ACCESS_TOKEN = "accessToken";
    private static final String KEY_API_CLIENT_ID = "clientId";
    private static final String KEY_API_USER_UID = "userUid";
    private static final String KEY_USER_ID = "userId";
    private static final String KEY_USER_EMAIL = "email";
    private static final String KEY_USER_FIRST_NAME = "firstName";
    private static final String KEY_USER_LAST_NAME = "lastName";

    private static Settings sInstance;
    private static Context sContext;


    private Settings(Context context) {
        sContext = context;
    }


    public static Settings getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new Settings(context);
        }
        return sInstance;
    }


    public SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(sContext);
    }


    public void createSession(String uid, String accessToken, String clientId, SessionAM session) {
        SharedPreferences.Editor editor = getPreferences().edit();

        editor.putString(KEY_API_USER_UID, uid);
        editor.putString(KEY_API_ACCESS_TOKEN, accessToken);
        editor.putString(KEY_API_CLIENT_ID, clientId);

        editor.putLong(KEY_USER_ID, session.getUser().getId());
        editor.putString(KEY_USER_FIRST_NAME, session.getUser().getFirstName());
        editor.putString(KEY_USER_LAST_NAME, session.getUser().getLastName());
        editor.putString(KEY_USER_EMAIL, session.getUser().getEmail());

        editor.commit();
    }


    public void destroySession() {
        SharedPreferences.Editor editor = getPreferences().edit();

        editor.putString(KEY_API_USER_UID, null);
        editor.putString(KEY_API_ACCESS_TOKEN, null);
        editor.putString(KEY_API_CLIENT_ID, null);

        editor.putLong(KEY_USER_ID, -1);
        editor.putString(KEY_USER_FIRST_NAME, null);
        editor.putString(KEY_USER_LAST_NAME, null);
        editor.putString(KEY_USER_EMAIL, null);

        editor.commit();

        gotoLoginScreen();
    }


    public String getAccessToken() {
        return getPreferences().getString(KEY_API_ACCESS_TOKEN, null);
    }


    public String getClientId() {
        return getPreferences().getString(KEY_API_CLIENT_ID, null);
    }


    public String getUserUid() {
        return getPreferences().getString(KEY_API_USER_UID, null);
    }


    public boolean isUserLoggedIn() {
        String accessToken = getAccessToken();
        String userUid = getUserUid();
        String clientId = getClientId();

        /*Log.d(TAG, "userUid: " + userUid);
        Log.d(TAG, "accessToken: " + accessToken);
        Log.d(TAG, "clientId: " + clientId);*/

        if (accessToken != null && userUid != null && clientId != null)
            return true;

        return false;
    }


    public void gotoLoginScreen() {
        Intent i = new Intent(sContext, SessionActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // close all activities from stack
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // start new activity
        sContext.startActivity(i);
        ((AppCompatActivity) sContext).finish();
    }
}
