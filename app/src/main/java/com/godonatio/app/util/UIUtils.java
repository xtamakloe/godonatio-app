package com.godonatio.app.util;


import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.godonatio.app.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Created by Christian Tamakloe on 28/04/2016.
 */
public class UIUtils {


    private static final String TAG = "DBG_UIUtils";


    public static void setHomeAsUpIndicator(Toolbar toolbar, final Activity activity) {
        // Show back arrow and handle navigation (works for LG bugs)
        toolbar.setNavigationIcon(
                ContextCompat.getDrawable(activity, R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });
    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }


    public static String formatMoney(String currency, String amount) {

        StringBuilder mask = new StringBuilder();
        mask.append("###,###,###");

        if (!(Double.parseDouble(amount) % 1 == 0.0))
            mask.append(".##");

        DecimalFormat formatter = new DecimalFormat(mask.toString());
        return currency + formatter.format(Double.parseDouble(amount));
    }


    public static String formatDate(String dateString) {

        DateTime receivedAt = new DateTime(dateString);
        String time = DateTimeFormat.forPattern("h:mm a").print(receivedAt);

        if (DateUtils.isToday(receivedAt.toDate().getTime())) {

            if (receivedAt.isAfter(DateTime.now().minusMinutes(1)))
                return "Just Now";
            else if (receivedAt.isAfter(DateTime.now().minusHours(3)))
                return DateUtils.getRelativeTimeSpanString(receivedAt.getMillis()).toString();
            else
                return String.format("Today at %s", time);

        } else {
            String month = receivedAt.monthOfYear().getAsShortText(Locale.getDefault());
            String day = Integer.toString(receivedAt.getDayOfMonth());
            String dayOfWeek = receivedAt.dayOfWeek().getAsShortText(Locale.getDefault());

            return String.format("%s, %s %s at %s", dayOfWeek, month, day, time);
        }
    }
}
