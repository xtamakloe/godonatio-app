package com.godonatio.app.util;


import android.support.v4.app.Fragment;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Christian Tamakloe on 04/04/2016.
 */
public class Utils {

    public static void registerEventBus(Fragment fragment) {
        if (!EventBus.getDefault().isRegistered(fragment))
            EventBus.getDefault().register(fragment);
    }
}
